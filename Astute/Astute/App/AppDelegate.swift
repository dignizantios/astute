//
//  AppDelegate.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//



/*
 
 if dot not show proper in Upper Side
 
 FSCalendarCell.m ->
 
 CGFloat eventSize = _shapeLayer.frame.size.height/2.0;
 
 _eventIndicator.frame = CGRectMake(
 self.preferredEventOffset.x+11,
 - 5,
 self.fs_width,
 eventSize
 );
 
 
 */


import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import LinkedinSwift
import GoogleSignIn
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //FB integration
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        IQKeyboardManager.shared.enable = true
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        
        if GlobalVariables.socialLogin == "0"
        {
            return  FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation:annotation)
        }
        else if GlobalVariables.socialLogin == "1"
        {
            if LinkedinSwiftHelper.shouldHandle(url) {
                return LinkedinSwiftHelper.application(application,
                                                       open: url,
                                                       sourceApplication: sourceApplication,
                                                       annotation: annotation
                )
            }
            
        }
        return true
        
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        if GlobalVariables.socialLogin == "1" // Linkedin
        {
            if LinkedinSwiftHelper.shouldHandle(url) {
                return LinkedinSwiftHelper.application(app, open: url, sourceApplication: nil, annotation: nil)
            }
        }
        else if GlobalVariables.socialLogin == "0" // facebbok
        {
            return  FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
        else if GlobalVariables.socialLogin == "2" // Google
        {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplication.OpenURLOptionsKey.annotation])
            
        }
        
        return false
        
    }


}

