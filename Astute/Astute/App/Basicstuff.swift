//
//  Basicstuff.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView
import MaterialComponents


struct GlobalVariables {
    
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    static var socialLogin = "" // 0 for FB
                                // 1 for Linkedin
                                // 2 for Google Plus
 
    static let networkMsg = getCommonString(key: "No_internet_connection_Please_try_again_later_key")
    
    static let serverNotResponding = getCommonString(key: "Server_not_responding_Please_try_again_later_key")
    
    static let userURL = "https://eventsservice.azurewebsites.net/User/"
    static let eventsURL = "https://eventsservice.azurewebsites.net/Events/"

    static let mainURL = "https://eventsservice.azurewebsites.net/App/"
    
    static let apiURL = "https://eventsservice.azurewebsites.net/api/"
    static let webApiURL = "http://www.pokagonband-nsn.gov/api/"
    
}



let appdelgate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard



//MARK: - Setup mapping

let StringFilePath = Bundle.main.path(forResource: "Language", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)

func getCommonString(key:String) -> String
{
    return dictStrings?.object(forKey: key) as? String ?? ""
}


//MARK: - Toaster

func makeToast(strMessage : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    
}


//MARK: - Storagae


func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    print(data)
    return data[forKey].stringValue
}




extension UIViewController : NVActivityIndicatorViewable
{
    
    //MARKL - Fonts
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - Alert
    
    func setAlert(msg:String)
    {
        let alert = UIAlertView()
        alert.title = "Pokagon Citizen Services"
        alert.message = msg
        alert.addButton(withTitle:"Ok")
        alert.show()
    }
    
    
    //MARK: - Validation email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    
    // MARK: -  For Loader NVActivityIndicatorView Process
    
    func showLoader()
    {
        let LoaderString:String = "Loading..."
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
        
        
    }
    
    func hideLoader()
    {
        stopAnimating()
    }
    
    //MARK: - Navigation Controller Setup
    
    
    func setupNavigationbar(titleText:String)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        
        self.navigationItem.hidesBackButton = true
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.appThemeStrongRedColor
        HeaderView.font = themeFont(size: 21, fontname: .light)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeStrongRedColor
        
        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    
    @objc func backButtonTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupNavigationbarwithBackButton(titleText:String)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_arrow_back_header") , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 18, fontname: .light)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeStrongRedColor
        
        self.navigationItem.titleView = HeaderView
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeStrongRedColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    
    //MARK: - Date and Time Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    
    
    
    
    func strTostr(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate = dateFormatterGet.date(from: strDate)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.string(from: strdate!)
        //    return dateFormatterPrint.date(from: date)!
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
}


extension UITableViewCell
{
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeStrongRedColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.contentView.endEditing(true)
    }
}
