//
//  ServiceListPostfix.swift
//  Hand2Home
//
//  Created by YASH on 03/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation


let register = "Register"
let Login = "Login"
let splashImages = "AppSettings"
let mainListing = "Menu"
let eventDataInCalendar = "month_event"
let eventMonthInCalendar = "days-with-events/"
//let eventOnParticularDate = "date_event"
let eventOnParticularDate = "events-on-day/"
let editProfileAPI = "Update"
let submitGuestDetails = "AddGuest"
let VerifyMobile = "VerifyMobile"
let VerifyEmail = "VerifyEmail"
let GetEventTypes = "GetEventTypes"
let GetEventsByType = "GetEventsByType"
let GetWordOfTheDay = "WordOfTheDay/List"
let GetCulturalHighlights = "cultural-highlights"
let GetHighlightDetails = "highlight-detail/"
let GetEventDetails = "event-detail/"
let GetUpcomingDeadlines = "upcoming-deadlines"
let GetTelephoneDirectory = "Staff/List"
let GetICSCalandarEvent = "download-event-detail/"
let GetUpcommingEvents = "upcoming-events"
