//
//  Service.swift
//  Hand2Home
//
//  Created by YASH on 02/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService {
    func Service(url:String,param : [String:Any],completion:@escaping(Result<JSON>)-> ()) {
        
        print("url - ",url)
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: "", password:"").responseSwiftyJSON(completionHandler: {
            
            if $0.result.isSuccess {
                completion($0.result)
            }
            else if $0.result.isFailure {
                let statusCode = $0.response?.statusCode
                print("StatusCode : \(statusCode)")
                if(statusCode == 500) {
                    
                }
                else if(statusCode != nil) {
                    /// APIResponseHandle(statusCode: statusCode!)
                    completion($0.result)
                }
                else {
                    makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                    completion($0.result)
                }
            }
            else {
                makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                //                    completion($0.result)
            }
        })
    }
    
    func GetService(url:String,param : [String:Any],completion:@escaping(Result<JSON>)-> ()) {
        
        print("url - ",url)
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: "", password:"").responseSwiftyJSON(completionHandler: {
            
            if $0.result.isSuccess {
                completion($0.result)
            }
            else if $0.result.isFailure {
                let statusCode = $0.response?.statusCode
                print("StatusCode : \(statusCode)")
                if(statusCode == 500) {
                    
                }
                else if(statusCode != nil) {
                    /// APIResponseHandle(statusCode: statusCode!)
                    completion($0.result)
                }
                else {
                    makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                    completion($0.result)
                }
            }
            else {
                makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                //                    completion($0.result)
            }
        })
    }
    
    func GetICSFILEService(url:String,param : [String:Any],completion:@escaping(Result<JSON>)-> ()) {
        
        print("url - ",url)
        let fileManager = FileManager.default
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        Alamofire.request("\(url)").downloadProgress(closure : { (progress) in
            print(progress.fractionCompleted)
            
        }).responseData{ (response) in
            print(response)
            print(response.result.value!)
            print(response.result.description)
           // let randomString = NSUUID().uuidString
            let randomString = param["id"] as! String
            if let data = response.result.value {
                
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let videoURL = documentsURL.appendingPathComponent("\(randomString).ics")
                //UIApplication.shared.open(videoURL, options: [:], completionHandler: nil)
                do {
                    try data.write(to: videoURL)
                    
                } catch {
                    print("Something went wrong!")
                }
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let url = NSURL(fileURLWithPath: path)
                print(url)
                if let pathComponent = url.appendingPathComponent("\("\(randomString).ics")") {
                    let filePath = pathComponent.path
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: filePath) {
                        print("FILE AVAILABLE")
                        //UIApplication.shared.open(pathComponent , options: [:], completionHandler: nil)
                        
                        let link = "https://\(filePath)"
                        let webcal = NSURL(string: link)
                        DispatchQueue.main.async {
                            UIApplication.shared.open(webcal as! URL, options: [:], completionHandler: nil)
                        }
                        
                        
 
                    } else {
                        print("FILE NOT AVAILABLE")
                    }
                } else {
                    print("FILE PATH NOT AVAILABLE")
                }
                
                
            }
        }
/*
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding.default, headers: ["Content-Type" :"text/calendar"]).response { (response) in
            
            print("response------>",response)
        }*/
        /*authenticate(user: "", password:"").responseSwiftyJSON(completionHandler: {
            
            if $0.result.isSuccess {
                completion($0.result)
            }
            else if $0.result.isFailure {
                let statusCode = $0.response?.statusCode
                print("StatusCode : \(statusCode)")
                if(statusCode == 500) {
                    
                }
                else if(statusCode != nil) {
                    /// APIResponseHandle(statusCode: statusCode!)
                    completion($0.result)
                }
                else {
                    makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                    completion($0.result)
                }
            }
            else {
                makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                //                    completion($0.result)
            }
        })*/
    }
    func uploadImagesService(url:String,img:UIImage,withName:String,param : [String:String],completion:@escaping(Result<JSON>)-> ()) {
        
        print("url - ",url)
        
        let unit64:UInt64 = 10_000_000
        let PasswordString =  String(format: "\(""):\("")")
        let PasswordData = PasswordString.data(using: .utf8)
        let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
        
        let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
        print("headers:==\(headers)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            let imgData = img.jpegData(compressionQuality: 0.7)
            
            multipartFormData.append(imgData!, withName: withName, fileName:"image" , mimeType: "image/png")
            
            for (key, value) in param {
                multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
            
        }, usingThreshold: unit64, to: url, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //send progress using delegate
                })
                upload.responseSwiftyJSON(completionHandler: {
                    print("Result : \($0.result)")
                    
                    if $0.result.isSuccess {
                        completion($0.result)
                    }
                    else if $0.result.isFailure {
                        let statusCode = $0.response?.statusCode
                        print("StatusCode : \(statusCode)")
                        if(statusCode == 500) {
                            
                        }
                        else if(statusCode != nil){
                            /// APIResponseHandle(statusCode: statusCode!)
                            completion($0.result)
                        }
                        else {
                            makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                            
                            //                                completion($0.result)
                        }
                    }
                    else {
                        makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                        
                        //                    completion($0.result)
                    }
                })
                
            case .failure(_):
                makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
            }
        })
    }
    
    
    
    /*
    func ServiceGETMethod(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        
        print("url - ",url)
        
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: basic_username, password:basic_password).responseSwiftyJSON(completionHandler:
            {
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    if(statusCode == 500)
                    {
                        
                    }else if(statusCode != nil)
                    {
                        /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        
                        makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                        
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                    
                    //                    completion($0.result)
                }
        })
    }
    */
    
}

