//
//  WordOfTheDayVc.swift
//  Astute
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage

class WordOfTheDayVc: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblWordOfDayList: UITableView!
    
    //MARK: - Variable
    var arrayDayListing : [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblWordOfDayList.register(UINib(nibName: "WordOfTheDayCell", bundle: nil), forCellReuseIdentifier: "WordOfTheDayCell")
        tblWordOfDayList.tableFooterView = UIView()
        self.setupUI()
        self.getAllWordOdTheDayList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Word_at_the_day_key"))
    }

    func setupUI() {
        
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                tblWordOfDayList.backgroundColor = userSelectedColor
                vwBack.backgroundColor = userSelectedColor
            }
        }
        
        lblTitle.text = getCommonString(key: "Word_at_the_day_key")
         lblTitle.font = themeFont(size: 20, fontname: .bold)
    }

}
//MARK: - TableView data Source

extension WordOfTheDayVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayDayListing.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordOfTheDayCell") as! WordOfTheDayCell
        
        cell.tag = indexPath.row
        var dict = self.arrayDayListing[indexPath.row]
        
        cell.lblTitle.text = dict["Title"].stringValue
        cell.lblDescription.text = dict["Meaning"].stringValue
        return cell
        
    }
    
}

//MARK: - APi calling

extension WordOfTheDayVc
{
    
    func getAllWordOdTheDayList()
    {
        
        var kURL = String()
        kURL = "\(GlobalVariables.apiURL)\(GetWordOfTheDay)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [String:String]()
            
            
            //print("parma \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                if let jsn = respones.value?.rawString() {
                    print("json \(jsn)")
                    let json = JSON(parseJSON: jsn)
                    if json["Flag"].stringValue == "1"
                    {
                        let data = json["data"]
                        print(data)
                        self.arrayDayListing = data.arrayValue
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                self.tblWordOfDayList.reloadData()
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
}
