//
//  WordOfTheDayCell.swift
//  Astute
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class WordOfTheDayCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.font = themeFont(size: 16, fontname: .bold)
        lblTitle.textColor = UIColor.appThemeStrongRedColor
        lblDescription.font = themeFont(size: 14, fontname: .light)
        lblDescription.textColor = UIColor.appThemeDarkYanColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
