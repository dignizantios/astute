//
//  PopupVc.swift
//  Astute
//
//  Created by YASH on 20/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit



protocol popupCommonDelegate  {
    
    func ProfileAction()
    
    func LogoutAction()
    
}

class PopupVc: UIViewController,UIGestureRecognizerDelegate {

    //MARK: - Outlet
    @IBOutlet weak var imageEvent: UIImageView!
    
    @IBOutlet weak var vwOption: UIView!
    
    @IBOutlet weak var vwProfile: UIView!
    @IBOutlet weak var lblProfile: UILabel!
    
    @IBOutlet weak var vwHeight: NSLayoutConstraint!
    //MARK: - Variable
    
    var popupDelegate : popupCommonDelegate?

    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        showAnimate()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        removeAnimate()
    }
    
    func setupUI()
    {
        self.vwHeight.constant = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(TappedGesture))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        self.imageEvent.image = self.imageEvent.image!.withRenderingMode(.alwaysTemplate)
        self.imageEvent.tintColor = UIColor.appThemeStrongRedColor
        
        [lblProfile].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeStrongRedColor
            lbl?.font = themeFont(size: 14, fontname: .light)
        }
        
    }
    
    
    

}

//MARK: - Animation
extension PopupVc
{
    
    func showAnimate()
    {
        
        self.vwOption.isHidden = false
        self.vwHeight.constant = 40
        
        UIView.animate(withDuration: 0.3, animations: {
            //  self.view.alpha = 1.0
            
            self.view.layoutIfNeeded()
            
        })
    }
    
    func removeAnimate()
    {
        
        self.vwHeight.constant = 0.0
        self.vwProfile.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            //  self.view.alpha = 1.0
            
            self.view.layoutIfNeeded()
            self.perform(#selector(self.dismisscontroller), with: nil, afterDelay: 0.3)
            
        })
    }
    
    
    @objc func dismisscontroller()
    {
        dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - IBAction

extension PopupVc
{
    
    
    @objc func TappedGesture()
    {
        removeAnimate()
    }
    
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        
        removeAnimate()
        
        popupDelegate?.ProfileAction()
        
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        removeAnimate()
        
        popupDelegate?.LogoutAction()
        
    }
    
}
