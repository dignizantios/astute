//
//  CulturalHighlightCell.swift
//  Astute
//
//  Created by Abhay on 01/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class CulturalHighlightCell: UITableViewCell {

    @IBOutlet weak var imgCulture: CustomImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        lblTitle.font = themeFont(size: 18, fontname: .bold)
        lblTitle.textColor = UIColor.appThemeStrongRedColor
    }
    override func layoutSubviews() {
        imgCulture.clipsToBounds = true
    }
    
}
