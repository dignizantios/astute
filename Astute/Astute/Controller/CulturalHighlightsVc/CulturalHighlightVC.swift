//
//  CulturalHighlightVC.swift
//  Astute
//
//  Created by Abhay on 01/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage

class CulturalHighlightVC: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblCulturalList: UITableView!
    
    //MARK: - Variable
    var arrayCulturalListing : [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblCulturalList.register(UINib(nibName: "CulturalHighlightCell", bundle: nil), forCellReuseIdentifier: "CulturalHighlightCell")
        tblCulturalList.tableFooterView = UIView()
        tblCulturalList.delegate = self
        tblCulturalList.dataSource = self
        self.setupUI()
        self.getAllCulturalHighlightList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Cultural_Highlights_key"))
    }
    
    func setupUI() {
        
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                tblCulturalList.backgroundColor = userSelectedColor
                vwBack.backgroundColor = userSelectedColor
            }
        }
        
        lblTitle.text = getCommonString(key: "Cultural_Highlights_key")
        lblTitle.font = themeFont(size: 20, fontname: .bold)
    }


}
//MARK: - TableView data Source

extension CulturalHighlightVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.arrayCulturalListing.count > 0{
            return 5
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CulturalHighlightCell") as! CulturalHighlightCell
        
        cell.tag = indexPath.row
       // print("dic-->",self.arrayCulturalListing[indexPath.row])
        var dict = self.arrayCulturalListing[indexPath.row]["highlight"]
        
        cell.lblTitle.text = dict["title"].stringValue
      
            if dict["main_image"].stringValue == nil
            {
                cell.imgCulture.image = UIImage(named:"placeholder_image_listing_screen")
            }else{
                let mainDic = dict["main_image"]
                let imgURL = mainDic["src"].stringValue
                cell.imgCulture.sd_setShowActivityIndicatorView(true)
                cell.imgCulture.sd_setIndicatorStyle(.gray)
                cell.imgCulture.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "placeholder_image_listing_screen"), options: .lowPriority, completed: nil)
            }
            
       
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EventDetailsVC") as! EventDetailsVC
        obj.isCulturalHighlightDetails = true
        obj.culturalId = self.arrayCulturalListing[indexPath.row]["highlight"]["id"].stringValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}

//MARK: - APi calling

extension CulturalHighlightVC
{
    
    func getAllCulturalHighlightList()
    {
        
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(GetCulturalHighlights)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [String:String]()
            
            
            //print("parma \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                //if let jsn = respones.value?.rawString() {
                if let jsn = respones.value
                {
                    print("json \(jsn)")
                    //let json = JSON(parseJSON: jsn)
                    if jsn["highlights"].arrayValue.count != 0
                    {
                        self.arrayCulturalListing = jsn["highlights"].arrayValue.reversed()
                    }
                    else {
                        makeToast(strMessage: GlobalVariables.serverNotResponding)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                self.tblCulturalList.reloadData()
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
}
