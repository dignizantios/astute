//
//  EmailVerificationPopupVC.swift
//  Pokagon Citizen Services
//
//  Created by Haresh Bhai on 26/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

protocol EmailVerificationDelegate: class {
    func EmailVerificationFinish()
}

class EmailVerificationPopupVC: UIViewController {
    
    @IBOutlet weak var txtCode: UITextField!
    
    weak var delegate:EmailVerificationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnVerifyClicked(_ sender: Any) {
        if self.txtCode.text! == "" {
            makeToast(strMessage: "Please enter Code")
        }
        else {
            self.CreateVerification()
        }
    }
    
    func CreateVerification() {
        let kURL = "\(GlobalVariables.userURL)\(VerifyEmail)"
        print(kURL)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            var param = [String:String]()
            param["userid"] = getUserDetail("Id") == "" ? "123" : getUserDetail("Id")
            param["code"] = self.txtCode.text!
            print("parma \(param)")
            
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    guard let rowdata = try? json.rawData() else {return}
                    Defaults.setValue(rowdata, forKey: "userDetails")
                    Defaults.synchronize()
                    self.delegate?.EmailVerificationFinish()
                    self.btnCloseClicked(self)
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
