//
//  ProfileVc.swift
//  Astute
//
//  Created by YASH on 20/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class ProfileVc: UIViewController, MobileVerificationDelegate, EmailVerificationDelegate {

    //MARK: - Outlet
    
    @IBOutlet weak var vwBack: UIView!
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var btnEditProfile: CustomButton!
    @IBOutlet weak var btnLogout: CustomButton!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhoneNUmber: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    
    @IBOutlet weak var btnVerifiedEmail: UIButton!
    @IBOutlet weak var btnVerifiedMobile: UIButton!

    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Profile_key"))
        
        setData()
        
    }
    
    func setData()
    {
        lblName.text = getUserDetail("Name")
        lblEmail.text = getUserDetail("EMail")
        lblLocation.text = getUserDetail("Address")
        lblPhoneNUmber.text = getUserDetail("Mobile")
        
        let imgURL = getUserDetail("Profile")
        imgProfile.sd_setShowActivityIndicatorView(true)
        imgProfile.sd_setIndicatorStyle(.gray)
        imgProfile.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "user_placeholder_profile_screen"), options: .lowPriority, completed: nil)
        
        if getUserDetail("EMailVerified") == "true" {
            self.btnVerifiedEmail.setTitle("Verified", for: .normal)
            self.btnVerifiedEmail.setTitleColor(UIColor.green, for: .normal)
            self.btnVerifiedEmail.isUserInteractionEnabled = false
        }
        if getUserDetail("MobileVerified") == "true" {
            self.btnVerifiedMobile.setTitle("Verified", for: .normal)
            self.btnVerifiedMobile.setTitleColor(UIColor.green, for: .normal)
            self.btnVerifiedMobile.isUserInteractionEnabled = false
        }
        
        if getUserDetail("Mobile") == "" {
            self.btnVerifiedMobile.isHidden = true
        }
        else {
            self.btnVerifiedMobile.isHidden = false
        }
        
        if getUserDetail("EMail") == "" {
            self.btnVerifiedEmail.isHidden = true
        }
        else {
            self.btnVerifiedEmail.isHidden = false
        }
    }
    
    func setupUI()
    {
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        
        btnEditProfile.setTitle(getCommonString(key: "Edit_Profile_key"), for: .normal)
        btnEditProfile.setTitleColor(UIColor.appThemeStrongRedColor, for: .normal)
        btnLogout.setTitleColor(UIColor.appThemeStrongRedColor, for: .normal)

        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                vwBack.backgroundColor = userSelectedColor
            }
        }
        
        [lblName,lblEmail,lblLocation,lblPhoneNUmber].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 16, fontname: .light)
        }
       
    }
 
    
    @IBAction func btnEditProfileTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVc") as! EditProfileVc
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        self.LogoutAction()
    }
    
    @IBAction func btnVerifiedEmailClicked(_ sender: Any) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EmailVerificationPopupVC") as! EmailVerificationPopupVC
        obj.delegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
    }
    
    @IBAction func btnVerifiedMobileClicked(_ sender: Any) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MobileVerificationPopupVC") as! MobileVerificationPopupVC
        obj.delegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
    }
    
    func MobileVerificationFinish() {
        self.setData()
    }
    
    func EmailVerificationFinish() {
        self.setData()
    }
    func LogoutAction() {
        self.perform(#selector(logoutAlert), with: nil, afterDelay: 0.5)
    }
    
    @objc func logoutAlert()
    {
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func logoutAPICalling()
    {
        
        Defaults.removeObject(forKey: "userDetails")
        
        //let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AstuteMainVc") as! AstuteMainVc
        
        let rearNavigation = UINavigationController(rootViewController: vc)
        AppDelegate.shared.window?.rootViewController = rearNavigation
        
    }
    
}
