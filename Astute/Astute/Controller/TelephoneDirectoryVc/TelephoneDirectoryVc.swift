//
//  TelephoneDirectoryVc.swift
//  Astute
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage

class TelephoneDirectoryVc: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var tblPhoneDirectoryList: UITableView!
    @IBOutlet weak var vwSearch: CustomView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    
    //MARK: - Variable
    var arrayDayListing : [JSON] = []
    var tempDayListing : [JSON] = []
    var isFilter = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblPhoneDirectoryList.register(UINib(nibName: "TelephoneDirectortyCell", bundle: nil), forCellReuseIdentifier: "TelephoneDirectortyCell")
        tblPhoneDirectoryList.tableFooterView = UIView()
        self.setupUI()
        self.getTelephoneDirectoryList()
        txtSearch.delegate = self
        txtSearch.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Telephone_directory_key"))
    }
    
    func setupUI() {
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                tblPhoneDirectoryList.backgroundColor = userSelectedColor
                vwBack.backgroundColor = userSelectedColor
            }
        }
        btnClose.isHidden = true
        btnClose.tintColor = UIColor.appThemeStrongRedColor
       // let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        //self.view.addGestureRecognizer(tapGesture)
        
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @objc func makeCall(_ sender: UIButton) {
        vwSearch.borderColors = UIColor.appThemeStrongRedColor
        let phone = arrayDayListing[sender.tag]["ContactNumber"].stringValue
        if phone != ""
        {
            phone.makeAColl()
        }
        
        
    }
    
    //MARK: - UIButton Action
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        txtSearch.text = ""
        arrayDayListing = []
        arrayDayListing = tempDayListing
        btnClose.isHidden = true
        tblPhoneDirectoryList.reloadData()
    }
    

}
//MARK: - UITextfeild data delegates
extension TelephoneDirectoryVc : UITextFieldDelegate
{
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        
        if textField.text?.count == 0
        {
            
            arrayDayListing = tempDayListing
            
           btnClose.isHidden = true
            tblPhoneDirectoryList.reloadData()
            
        }
        else
        {
            btnClose.isHidden = false
            print("Text : \(textField.text)")
            let arr = NSMutableArray()
            //  print("arrayContact : \(arrJobData)")
            for i in 0..<self.tempDayListing.count{
                let dict = tempDayListing[i].dictionaryObject
                arr.add(dict)
            }
            
            //     print("Arrrr : \(arr)")
            //  arrOfAutoComplete = NSArray()
            let pre:NSPredicate = NSPredicate(format: "Name CONTAINS[c] %@", textField.text!)
            
            let arrOfAutoComplete = arr.filtered(using: pre) as NSArray
            //      print("arr : \(JSON(arrOfAutoComplete))")
            
            arrayDayListing = []
            
            arrayDayListing = JSON(arrOfAutoComplete).arrayValue
            
            //     print("arraySearch : \(arrJobData)")
            
            tblPhoneDirectoryList.reloadData()
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

//MARK: - TableView data Source

extension TelephoneDirectoryVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayDayListing.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TelephoneDirectortyCell") as! TelephoneDirectortyCell
        
        cell.tag = indexPath.row
        var dict = self.arrayDayListing[indexPath.row]
        let imgURL = dict["ProfileImage"].stringValue
        cell.imgProfile.sd_setShowActivityIndicatorView(true)
        cell.imgProfile.sd_setIndicatorStyle(.gray)
        cell.imgProfile.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "user_placeholder_profile_screen"), options: .lowPriority, completed: nil)
        cell.lblName.text = dict["Name"].stringValue
        cell.lblCantactNumber.text = dict["ContactNumber"].stringValue
        cell.btncCall.tag = indexPath.row
        cell.btncCall.addTarget(self, action: #selector(makeCall),
                                for:.touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "PhoneDetailsVc") as! PhoneDetailsVc
        obj.PhonesDetails = self.arrayDayListing[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}

//MARK: - APi calling

extension TelephoneDirectoryVc
{
    
    func getTelephoneDirectoryList()
    {
        
        var kURL = String()
        kURL = "\(GlobalVariables.apiURL)\(GetTelephoneDirectory)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [String:String]()
            
            
            //print("parma \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                if let jsn = respones.value?.rawString() {
                    print("json \(jsn)")
                    let json = JSON(parseJSON: jsn)
                    if json["Flag"].stringValue == "1"
                    {
                        let data = json["data"]
                        print(data)
                        self.arrayDayListing = data.arrayValue
                        self.tempDayListing = data.arrayValue
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                self.tblPhoneDirectoryList.reloadData()
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
}
//MARk:- Make call
extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeAColl() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}
