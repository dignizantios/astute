//
//  TelephoneDirectortyCell.swift
//  Astute
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TelephoneDirectortyCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCantactNumber: UILabel!
    @IBOutlet weak var btncCall: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfile.layer.cornerRadius =  imgProfile.frame.width/2
        
        lblName.font = themeFont(size: 15, fontname: .bold)
        lblName.textColor = UIColor.appThemeStrongRedColor
        
        lblCantactNumber.font = themeFont(size: 13, fontname: .light)
        lblCantactNumber.textColor = UIColor.appThemeDarkYanColor
        
        imgProfile.layer.cornerRadius =  imgProfile.frame.width/2
        imgProfile.clipsToBounds = true
        
        
        
        
    }
    
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        
        
    }
    
}
