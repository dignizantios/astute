//
//  PhoneDetailsVc.swift
//  Astute
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage
import MessageUI

class PhoneDetailsVc: UIViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var vwPhone: UIView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    
    //MARK: - Variable
    var PhonesDetails = JSON()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Telephone_directory_key"))
    }

    func setupUI() {
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                //tblWordOfDayList.backgroundColor = userSelectedColor
                vwBack.backgroundColor = userSelectedColor
            }
        }
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2.0
        imgProfile.clipsToBounds = true
        lblUserName.textColor = UIColor.appThemeStrongRedColor
        lblUserName.text = getCommonString(key: "Word_at_the_day_key")
        lblUserName.font = themeFont(size: 20, fontname: .bold)
        
        let imgURL = PhonesDetails["ProfileImage"].stringValue
        imgProfile.sd_setShowActivityIndicatorView(true)
        imgProfile.sd_setIndicatorStyle(.gray)
        imgProfile.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "user_placeholder_profile_screen"), options: .lowPriority, completed: nil)
        
        
        lblUserName.text = PhonesDetails["Name"].stringValue
        lblPosition.text = PhonesDetails["Position"].stringValue
        lblDepartment.text = PhonesDetails["Department"].stringValue
        
        lblPosition.textColor = UIColor.appThemeDarkYanColor
        lblPosition.font = themeFont(size: 18, fontname: .light)
        
        lblDepartment.textColor = UIColor.appThemeBlackColor
        lblDepartment.font = themeFont(size: 18, fontname: .light)
        
        lblPhoneNumber.textColor = UIColor.appThemeDarkYanColor
        lblPhoneNumber.font = themeFont(size: 28, fontname: .light)
        
        if PhonesDetails["ContactNumber"].stringValue != ""
        {
            lblPhoneNumber.text = PhonesDetails["ContactNumber"].stringValue
        }else{
            lblPhoneNumber.text = ""
            vwPhone.isHidden = true
        }
        
        if PhonesDetails["EmailId"].stringValue != ""
        {
            lblEmail.text = PhonesDetails["EmailId"].stringValue
        }else{
            lblEmail.text = ""
            vwEmail.isHidden = true
        }
        
        
    }
    //MARK:- Button Actions
    
    @IBAction func btnMakeCallAction(_ sender: UIButton) {
        let phone = PhonesDetails["ContactNumber"].stringValue
        if phone != ""
        {
            phone.makeAColl()
        }
        
    }
    
    @IBAction func btnEmailAction(_ sender: UIButton) {
        if PhonesDetails["EmailId"].stringValue != ""
        {
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self;
                mail.setCcRecipients(["\(PhonesDetails["EmailId"].stringValue)"])
                mail.setSubject("Your messagge")
                mail.setMessageBody("Message body", isHTML: false)
                /* let imageData: NSData = UIImagePNGRepresentation(imageView.image)!
                 mail.addAttachmentData(imageData, mimeType: "image/png", fileName: "imageName.png")*/
                self.present(mail, animated: true, completion: nil)
            }
        }
        
    }
}
