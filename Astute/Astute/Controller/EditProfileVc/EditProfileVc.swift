//
//  EditProfileVc.swift
//  Astute
//
//  Created by YASH on 20/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import TOCropViewController
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class EditProfileVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPhone: CustomTextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtvwAddress: CustomTextview!
    @IBOutlet weak var btnSave: CustomButton!
    
    //MARK: - Variable
    
    private var customImagePicker = CustomImagePicker()

    var imgSelected = "0"
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationbarwithBackButton(titleText:getCommonString(key: "Edit_Profile_key"))
    }
    
    
    func setupUI()
    {
        
        
        
        
        
        
        
        
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.layer.masksToBounds = true
        [txtName,txtEmail,txtPhone].forEach { (txt) in
            txt?.placeholder = getCommonString(key: "Enter_here_key")
            txt?.leftPaddingView = 15
            txt?.cornerRadius = 3
            if txt == txtName {
                txt?.backgroundColor = UIColor.hexStringToUIColor(hex: "969696").withAlphaComponent(0.3)
            }
            else {
                txt?.backgroundColor = UIColor.appThemeLightPurpleColor
            }
            txt?.layer.borderColor = UIColor.appThemeStrongRedColor.cgColor
            txt?.layer.borderWidth = 0.5
            txt?.font = themeFont(size: 15, fontname: .light)
            txt?.textColor = UIColor.appThemeBlackColor
            txt?.placeHolderColor = UIColor.appThemeBlackColor
            txt?.delegate = self
        }
        
        txtvwAddress.textColor = UIColor.appThemeBlackColor
        txtvwAddress.font = themeFont(size: 15, fontname: .light)
        txtvwAddress.backgroundColor = UIColor.appThemeLightPurpleColor
        txtvwAddress.layer.borderColor = UIColor.appThemeStrongRedColor.cgColor
        txtvwAddress.layer.borderWidth = 0.5
        txtvwAddress.contentInset = UIEdgeInsets(top: 2, left: 15, bottom: 2, right: 15)
        txtvwAddress.cornerRadius = 3
        
        lblPlaceholder.textColor = UIColor.black
        lblPlaceholder.font = themeFont(size: 15, fontname: .light)
        lblPlaceholder.text = getCommonString(key: "Enter_here_key")
        
        [lblName,lblEmail,lblPhone,lblAddress].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeStrongRedColor
            lbl?.font = themeFont(size: 15, fontname: .light)
        }
        
        lblName.text = getCommonString(key: "Name_key")
        lblEmail.text = getCommonString(key: "Email_key")
        lblPhone.text = getCommonString(key: "Phone_key")
        lblAddress.text = getCommonString(key: "Address_key")
        
        btnSave.setTitle(getCommonString(key: "SAVE_key"), for: .normal)
        btnSave.setupThemeButtonUI()
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                vwBack.backgroundColor = userSelectedColor
            }
        }
        
        addDoneButtonOnKeyboard(textfield: txtPhone)
        
        setData()
    }
    
    
    func setData()
    {
        txtName.text = getUserDetail("Name")
        txtEmail.text = getUserDetail("EMail")
        
        if getUserDetail("Address") != nil
        {
             lblPlaceholder.isHidden = true
             txtvwAddress.text = getUserDetail("Address")
        }
        else
        {
            lblPlaceholder.isHidden = false
        }
       
        txtPhone.text = getUserDetail("Mobile")
        
        if getUserDetail("Profile") != nil
        {
            
            imgSelected = "1"
            
            let imgURL = getUserDetail("Profile")
            
            imgProfile.sd_setShowActivityIndicatorView(true)
            imgProfile.sd_setIndicatorStyle(.gray)
            imgProfile.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "user_placeholder_profile_screen"), options: .lowPriority, completed: nil)
        }
        else
        {
            imgSelected = "0"
        }
       
    }
    
}

//MARK: - IBAction

extension EditProfileVc
{
    
    @IBAction func btnProfileSelectionTapped(_ sender: UIButton)
    {
        
        self.view.endEditing(true)
        selectImages()
        
    }
    
    @IBAction func btnSaveTappedAction(_ sender: UIButton) {
        
        if imgSelected == "0"
        {
            makeToast(strMessage: getCommonString(key: "Please_select_Photo_key"))
        }
        else if (txtName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_name_key"))
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email_key"))
        }
        else if (txtPhone.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_phone_number_key"))
        }
//        else if (txtPhone.text?.characters.count)! < GlobalVariables.phoneNumberLimit
//        {
//            makeToast(strMessage: getCommonString(key: "Please_enter_valid_phone_number_key"))
//        }
        else if txtPhone.text?.isNumeric == false
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_phone_number_key"))
        }
        else if (txtvwAddress.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: "Please_enter_address_key")
        }
        else
        {
            self.editProfile()
        }
    }
    
}

//MARK: - TextField Delegate

extension EditProfileVc: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}

extension EditProfileVc : UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            txtvwAddress.resignFirstResponder()
            return false
        }
        return true
    }
    
}

//MARK: - Image selection


extension EditProfileVc: TOCropViewControllerDelegate
{
    
    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appThemeStrongRedColor,
                                          imagePicked: { (response) in
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage.rawValue] as! UIImage
                                            
                                            print("theImage : \(theImage)")
                                            
                                            self.openCropVC(Image: theImage)
                                            
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
    
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
       
        imgSelected = "1"
        
        imgProfile.image = image
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        print("cancelled:\(cancelled)")
        
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - API calling

extension EditProfileVc
{
    
    func editProfile()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(GlobalVariables.userURL)\(editProfileAPI)"
            
            print("URL: \(url)")
            
            let param = [
                         "userId" : getUserDetail("Id") == "" ? "123" : getUserDetail("Id"),
                         "name" : txtName.text ?? "",
                         "email" :txtEmail.text ?? "",
                         "mobile" : txtPhone.text ?? "",
                         "address" : txtvwAddress.text ?? ""
                         
                         ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().uploadImagesService(url: url, img: imgProfile.image ?? UIImage(), withName: "profile", param: param, completion: { (response) in
                
                self.hideLoader()
                
                if let valueJSON = response.value
                {
                    print(valueJSON)
                    
                    let data = valueJSON["data"]
                    
                    if valueJSON["Flag"].stringValue == "1"
                    {
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        self.navigationController?.popViewController(animated: true)
                        
                        makeToast(strMessage: valueJSON["Message"].stringValue)
                        
                    }
                    else
                    {
                        makeToast(strMessage: valueJSON["Message"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: GlobalVariables.serverNotResponding)
                }
                
            })
            
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
}
