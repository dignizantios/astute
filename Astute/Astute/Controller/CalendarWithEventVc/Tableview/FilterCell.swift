//
//  FilterCell.swift
//  Pokagon Citizen Services
//
//  Created by Haresh Bhai on 26/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnRadio: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblName.textColor = UIColor.appThemeStrongRedColor
        lblName.font = themeFont(size: 14, fontname: .light)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
