//
//  CalednarWithEventVc.swift
//  Astute
//
//  Created by YASH on 20/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import FSCalendar
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON


class CalednarWithEventVc: UIViewController, FilterDelegate {
    
    //MARK: - Outlet
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var tblEventList: UITableView!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    //MARK: - Variable
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter    }()
    
    var arrayOfEvents : [String] = []
    var selectedDate = Date()
    var arrayOfEventsInParticularDate : [JSON] = []
    var mainIcon = UIImage()
    var selectedFilter: String = ""
    var isFilter:Bool = false
    var filterList : JSON = JSON()
    var tempArrayOfEventsInParticularDate : [JSON] = []
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        setupCalender()
        
        tblEventList.register(UINib(nibName: "FirstEventTableCell", bundle: nil), forCellReuseIdentifier: "FirstEventTableCell")
        tblEventList.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
        tblEventList.tableFooterView = UIView()
        
        
        let date = Date()
        let components = Calendar.current.dateComponents([.month,.year], from: date)
        let year =  components.year
        let month = components.month

        print("Year : \(year)")
        print("month:\(month)")
        
        self.evntListInCalendarAPICalling(month: month!, year: year!)
        
        //let value = DateToString(Formatter: "dd-MMM-yy", date: selectedDate)
        let value = DateToString(Formatter: "YYYMMDD", date: selectedDate)

        self.getListOfParticularDate(strDate: value)
        self.getFilterList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "EVENT_key"))
        
        let rightButton1 = UIBarButtonItem(image: UIImage(named: "ic_more_option_header") , style: .plain, target: self, action: #selector(btnMoreTapped))
        rightButton1.tintColor = .white
        
        let rightButton2 = UIBarButtonItem(image: UIImage(named: "ic_filter_icon-1") , style: .plain, target: self, action: #selector(btnFilterTapped))
        rightButton2.tintColor = UIColor(displayP3Red: 138.0/255.0, green: 151.0/255.0, blue: 94.0/255.0, alpha: 1.0)
        //rightButton2.tintColor = .white
        self.navigationItem.rightBarButtonItems = [rightButton1,rightButton2]
        
    }
    
    func setupUI()
    {
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                tblEventList.backgroundColor = userSelectedColor
            }
        }
        
        
    }
    
    func setupCalender()
    {
        
        calendar.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        calendar.headerHeight = 50.0
        calendar.weekdayHeight = 50.0
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        
        calendar.dataSource = self
        calendar.delegate = self
        
        calendar.appearance.weekdayTextColor = UIColor.appThemeLightGrayColor
        calendar.appearance.weekdayFont = themeFont(size: 15, fontname: .light)
        
        calendar.allowsMultipleSelection = false
        
        self.calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesUpperCase]

        calendar.pagingEnabled = true
        calendar.appearance.titleTodayColor = .black
        
        calendar.appearance.titleFont = themeFont(size: 16, fontname: .light)
        calendar.appearance.titleDefaultColor = .black
        
        calendar.appearance.headerTitleFont = themeFont(size: 15, fontname: .light)
        calendar.appearance.headerTitleColor = UIColor.appThemeStrongRedColor
        
        calendar.rowHeight = 45
        calendar.placeholderType = .none
        calendar.appearance.todayColor = .clear
        calendar.clipsToBounds = true // Remove top/bottom line
        calendar.appearance.titleSelectionColor = .black
        calendar.swipeToChooseGesture.isEnabled = false // Swipe-To-Choose
        
        calendar.scrollEnabled = true
        
    }
    
}


//MARK: - IBAction

extension CalednarWithEventVc
{
    
    @objc func btnMoreTapped()
    {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "PopupVc") as! PopupVc
        obj.popupDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
        
    }
    
    @objc func btnFilterTapped()
    {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.delegate = self
        obj.selectedFilter = self.selectedFilter
        obj.filterList = self.filterList
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
        
    }
    
    func FilterDidFinish(data: String) {
        print(data)
        self.isFilter = true
        self.selectedFilter = data
        if data == "" {
            //self.getListOfParticularDate(strDate: formatter.string(from: selectedDate))
            self.arrayOfEventsInParticularDate = []
            self.arrayOfEventsInParticularDate = self.tempArrayOfEventsInParticularDate
        }
        else {
            //self.makeFilter(eventType: data)
            self.arrayOfEventsInParticularDate = []
            self.arrayOfEventsInParticularDate = self.tempArrayOfEventsInParticularDate.filter({
                
                let category = $0["event"]["category"].stringValue.lowercased().components(separatedBy: ",").map({$0.trimmingCharacters(in: .whitespaces)})
                if category.contains(selectedFilter.lowercased().trimmingCharacters(in: .whitespaces)){
                    return true
                }
                return false
               
            })
            
            print("self.arrayOfEventsInParticularDate-->",self.arrayOfEventsInParticularDate)
        }
        self.tblEventList.reloadData()
    }
    
    
    @IBAction func btnPreviousMonthTappedAction(_ sender: UIButton) {
        
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = -1 // For prev button
        
        let currentPg = calendar.currentPage
        
        let gregorian = NSCalendar.init(calendarIdentifier: .gregorian)
        
        let month = _calendar.component(.month, from: currentPg)
        let year = _calendar.component(.year, from: currentPg)
        
        print("month : \(month-1)")
        print("year : \(year)")
        
        if month-1 == 0
        {
            //API calling
            self.evntListInCalendarAPICalling(month: 12, year: year - 1)
            //theModel.geHistoryList(month: 12 , year: year - 1)
            // month 12
            // year - 1
        }
        else
        {
            //APi calling
            self.evntListInCalendarAPICalling(month: month-1, year: year)
        }
        
        let currentPage = _calendar.date(byAdding: dateComponents, to:currentPg)
        
        self.calendar.setCurrentPage(currentPage!, animated: true)
        
    }
    
    @IBAction func btnNextMonthTappedAction(_ sender: UIButton) {
        
        let _calendar = Calendar.current
        
        var dateComponents = DateComponents()
        dateComponents.month = 1 // For next button
        
        let currentPg = calendar.currentPage
        
        let month = Calendar.current.component(.month, from: currentPg)
        let year = Calendar.current.component(.year, from: currentPg)
        
        print("month:\(month+1)")
        print("year:\(year)")
        
        if month+1 == 13
        {
            print("month : 1")
            print("year:\(year+1)")
            //APi calling
            self.evntListInCalendarAPICalling(month: 1, year: year+1)
            
        }
        else
        {
            //APi calling
            self.evntListInCalendarAPICalling(month: month+1, year: year)
        }
        
        let currentPage = _calendar.date(byAdding: dateComponents, to:currentPg)
        self.calendar.setCurrentPage(currentPage!, animated: true)
        
    }
    
    @IBAction func btnMonthYearSelectinTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MonthYearSelectionVc") as! MonthYearSelectionVc
        obj.pickerDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
        
    }
    
}


//MARK: - Tableview Delegate

extension CalednarWithEventVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if arrayOfEventsInParticularDate.count == 0
        {
           /*let lbl = UILabel(frame: CGRect(x: 0, y: 300, width: UIScreen.main.bounds.width, height: 50))
            lbl.text = "No Event Found."
            lbl.textAlignment = NSTextAlignment.center
            lbl.backgroundColor = UIColor.red
            lbl.textColor = .black
           // lbl.center = tableView.center
            tableView.backgroundView = lbl*/
            return 1
        }
        tableView.backgroundView = nil
        return arrayOfEventsInParticularDate.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if arrayOfEventsInParticularDate.count == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            cell.textLabel?.text = "No Event Found"
            cell.textLabel?.textAlignment = .center
            return cell;
            
        }else{
            let dict = arrayOfEventsInParticularDate[indexPath.row]["event"]
            
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FirstEventTableCell") as! FirstEventTableCell
                print(dict)
                cell.lblEventName.text = dict["title"].stringValue
                cell.lblLocation.text = dict["event_location"].stringValue
                // let time = dict["Time"].stringValue
                // cell.lblTime.text = strTostr(OrignalFormatter: "HH:mm", YouWantFormatter: "hh:mm a", strDate: time)
                cell.lblTime.text = dict["event_date"].stringValue
                
                cell.lblEventTitleOutSide.text = getCommonString(key: "Event_Key")
                
                let value = DateToString(Formatter: "dd-MMM-yy", date: selectedDate)
                cell.lblDateMonthYear.text = value
                
                cell.lblDay.text = DateToString(Formatter: "dd", date: selectedDate)
                cell.lblMonth.text = DateToString(Formatter: "MMMM", date: selectedDate)
                
                //            if dict["Images"].stringValue != "null"
                //            {
                //
                //                if dict["Images"].arrayValue.count > 0
                //                {
                //                    cell.imgEvent.sd_setShowActivityIndicatorView(true)
                //                    cell.imgEvent.sd_setIndicatorStyle(.gray)
                //                    if dict["Images"][0]["event_images"].url == nil{
                //                        cell.imgEvent.image = mainIcon//UIImage(named: "placeholder_image_banner")
                //                    }else{
                //                         cell.imgEvent.sd_setImage(with: dict["Images"][0]["event_images"].url, placeholderImage: UIImage(named: "placeholder_image_listing_screen"), options: .lowPriority, completed: nil)
                //                    }
                //
                //                }else{
                //                    cell.imgEvent.image = mainIcon//UIImage(named: "placeholder_image_banner")
                //                }
                //
                //            }else{
                //                cell.imgEvent.image = mainIcon//UIImage(named: "placeholder_image_banner")
                //            }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as! EventCell
                cell.imgForwordArrow.image = UIImage(named: "ic_arrow_calender_listing_screen-1")
                cell.lblEventName.text = dict["title"].stringValue
                cell.lblLocation.text = dict["event_location"].stringValue
                /*let time = dict["Time"].stringValue
                 cell.lblTime.text = strTostr(OrignalFormatter: "HH:mm", YouWantFormatter: "hh:mm a", strDate: time)*/
                cell.lblTime.text = dict["event_date"].stringValue
                cell.lblDay.text = DateToString(Formatter: "dd", date: selectedDate)
                cell.lblMonth.text = DateToString(Formatter: "MMMM", date: selectedDate)
                cell.viewDay.isHidden = false
                cell.imgEvent.isHidden = true
                //            if dict["Images"].stringValue != "null"
                //            {
                //
                //                if dict["Images"].arrayValue.count > 0
                //                {
                //                    cell.imgEvent.sd_setShowActivityIndicatorView(true)
                //                    cell.imgEvent.sd_setIndicatorStyle(.gray)
                //                    cell.imgEvent.sd_setImage(with: dict["Images"][0]["event_images"].url, placeholderImage: UIImage(named: "placeholder_image_listing_screen"), options: .lowPriority, completed: nil)
                //                }
                //
                //            }
                
                return cell
            }
            
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrayOfEventsInParticularDate[indexPath.row]
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EventDetailsVC") as! EventDetailsVC
        obj.selectedController = .fromCalendarEventVc
        obj.dictDetails = dict["event"]
        obj.stringDate = selectedDate
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}


//MARK: - Popup Delegate

extension CalednarWithEventVc : popupCommonDelegate
{
    func ProfileAction() {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EventSubscriptionVC") as! EventSubscriptionVC
        
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    func LogoutAction() {
        
        self.perform(#selector(logoutAlert), with: nil, afterDelay: 0.5)
        
    }
    
    @objc func logoutAlert()
    {
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }    
}

//MARK: - API calling
extension CalednarWithEventVc
{
    
    func logoutAPICalling()
    {
        
        Defaults.removeObject(forKey: "userDetails")
        
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        
        let rearNavigation = UINavigationController(rootViewController: vc)
        AppDelegate.shared.window?.rootViewController = rearNavigation
        
    }
    
    
    func evntListInCalendarAPICalling(month : Int, year : Int)
    {
        
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(eventMonthInCalendar)\(year)\(month)"//"\(eventDataInCalendar)"
        
        print("AURL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = [String:String]()
            /*var param =  [
                "userid" : getUserDetail("Id") == "" ? "123" : getUserDetail("Id"),
                "Month" : "\(month)",
                "Year" : "\(year)"
                ]*/
            
            
            print("parma \(param)")
            
//            self.showLoader()
            
            CommonService().GetService(url: kURL, param: param, completion: { (respones) in
                
//                self.hideLoader()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["event_dates"].exists()
                    {
                        let data = json["event_dates"].arrayValue
                        
                        self.arrayOfEvents = []
                        
                        for i in 0..<data.count
                        {
                            
                            if data[i]["event_date"].exists()
                            {
                                let date = self.strTostr(OrignalFormatter: "MM-dd-yyyy", YouWantFormatter: "yyyy-MM-dd", strDate: data[i]["event_date"]["date"].stringValue.trimmingCharacters(in: .whitespaces))
                                self.arrayOfEvents.append(date)
                            }
                            
                        }
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                
                self.calendar.reloadData()
                
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
    
    func getListOfParticularDate(strDate : String)
    {
        self.isFilter = false
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(eventOnParticularDate)\(strDate)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = [String:String]()
            /*var param =  [
                "userid" : getUserDetail("Id") == "" ? "123" : getUserDetail("Id"),
                "date" : strDate
                
            ]*/
            
            
            print("parma \(param)")
            
            self.showLoader()
            
            CommonService().GetService(url: kURL, param: param, completion: { (respones) in
                
            self.hideLoader()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["events"].exists()
                    {
                        let data = json["events"].arrayValue
                        self.arrayOfEventsInParticularDate = []
                        self.arrayOfEventsInParticularDate = data
                        self.tempArrayOfEventsInParticularDate = data
                       /* var tempFilter:[String] = []
                        for i in 0..<self.tempArrayOfEventsInParticularDate.count{
                            let dic = self.tempArrayOfEventsInParticularDate[i]["event"]
                            let strCategory  = dic["category"].stringValue
                            let categoryArray = strCategory.components(separatedBy: ",")
                            if categoryArray.count > 1{
                                 for j in 0..<categoryArray.count{
                                    
                                    tempFilter.append(categoryArray[j].trimmingCharacters(in: .whitespaces))
                                }
                            }else{
                                tempFilter.append(strCategory.trimmingCharacters(in: .whitespaces))
                            }
                            
                        }
                        print("tempFilter==",tempFilter)
                        self.filterList = JSON(Array(Set(tempFilter)))
                        print(self.filterList)*/
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                
               self.tblEventList.reloadData()
                
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
    
    func makeFilter(eventType: String) {
        let kURL = "\(GlobalVariables.eventsURL)\(GetEventsByType)"
        print(kURL)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            var param = [String:String]()
            
            param["eventType"] = eventType
            
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1" {
                        let data = json["data"].arrayValue
                        self.arrayOfEventsInParticularDate = []
                        self.arrayOfEventsInParticularDate = data
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                self.tblEventList.reloadData()
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    func getFilterList() {
        let kURL = "\(GlobalVariables.webApiURL)\(GetUpcommingEvents)"
        print(kURL)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let param = [String:String]()
            
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["events"].exists() {
                        let data = json["events"].arrayValue
                        var tempFilter:[String] = []
                        for i in 0..<data.count{
                            let dic = data[i]["event"]
                            let strCategory  = dic["category"].stringValue
                            if strCategory != ""
                            {
                                let categoryArray = strCategory.components(separatedBy: ",")
                                if categoryArray.count > 1{
                                    for j in 0..<categoryArray.count{
                                        
                                        tempFilter.append(categoryArray[j].trimmingCharacters(in: .whitespaces))
                                    }
                                }else{
                                    tempFilter.append(strCategory.trimmingCharacters(in: .whitespaces))
                                }
                            }
                            
                        }
                        //print("tempFilter==",tempFilter)
                        self.filterList = JSON(Array(Set(tempFilter)))
                        //print("filterList==",self.filterList)
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
}



// MARK:- FSCalendarDelegate , Data Source

extension CalednarWithEventVc : FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance
{
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let strDate = self.formatter.string(from:date)
        
        
        if arrayOfEvents.contains(strDate)
        {
            return 1
        }
        
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]?
    {

        let strDate = formatter.string(from: date)
        
        if arrayOfEvents.contains(strDate)
        {
            return [UIColor.red]
        }
        
        return [UIColor.clear]
        
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventSelectionColorsFor date: Date) -> [UIColor]? {
        
        let strDate = formatter.string(from: date)
        
        if arrayOfEvents.contains(strDate)
        {
            return [UIColor.red]
        }
        
        return [UIColor.clear]
        
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        
        return .clear
        
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor?
    {
        return UIColor.appThemeStrongRedColor
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        
        /* if(selectedFillDates.contains(self.formatter.string(from:date)))
         {
         return false
         }*/
        
        
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //print("selectedDAte yyyy-MM-dd HH:mm:ss -->\(date)")
        print("did select date \(formatter.string(from: date))")
        
        let strDate = formatter.string(from: date)
        selectedDate = date
        let value = DateToString(Formatter: "yyyyMMdd", date: selectedDate)
        self.getListOfParticularDate(strDate: value)
        
        
        /*
        if theModel.arrDateWithHours.contains(strDate) || theModel.arrDateWithAbsence.contains(strDate) || theModel.arrOverly.contains(strDate) // All
        {
            theModel.clickOnDateGetDetails(date: strDate)
        }
        */
        
    }
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        
        let now = calendar.currentPage
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let nameOfyear = dateFormatter.string(from: now)
        
        dateFormatter.dateFormat = "MM"
        let nameOfMonth = dateFormatter.string(from: now)
        print("name of the Year : \(nameOfyear)")
        print("name of the Month : \(nameOfMonth)")
        
        self.evntListInCalendarAPICalling(month: Int(nameOfMonth)!, year: Int(nameOfyear)!)
        
    }
}


extension CalednarWithEventVc: MonthYearPickerDelegate
{
    func selectedMonthYear(monthName: String, monthInt: Int, yearName: String) {
        
        print("month : \(monthName) monthIndex:\(monthInt+1) year : \(yearName)")
        
        let strStartDate = "01-\(monthName)-\(yearName)"
        
        let currentPageDate = calendar.currentPage
        let dateValue = stringTodate(Formatter: "dd-MM-yyyy", strDate: strStartDate)
        
        let monthDiffrence = currentPageDate.interval(ofComponent: .month, fromDate: dateValue)
        
        let nextMonth = Calendar.current.date(byAdding: .month, value: -(monthDiffrence) , to: currentPageDate)!
        calendar.setCurrentPage(nextMonth, animated: false)
        
        self.evntListInCalendarAPICalling(month: monthInt+1, year: Int(yearName)!)
        
    }
   
}

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        print("start - ",start)
        print("end - ",end)
        return end - start
    }
    
}



