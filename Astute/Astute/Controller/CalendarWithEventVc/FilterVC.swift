//
//  FilterVC.swift
//  Pokagon Citizen Services
//
//  Created by Haresh Bhai on 26/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

protocol FilterDelegate: class {
    func FilterDidFinish(data: String)
}

class FilterVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vwTopView: UIView!
    
    @IBOutlet weak var btnFilter: CustomButton!
    
    var filterList : JSON = JSON()
    weak var delegate: FilterDelegate?
    var selectedFilter: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
        tableView.tableFooterView = UIView()
        //self.getFilter()
        self.createHeight(total: (self.filterList.array?.count)!)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        btnFilter.backgroundColor = UIColor.appThemeStrongRedColor
        vwTopView.backgroundColor = UIColor.appThemeStrongRedColor
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterList.array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FilterCell = self.tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
        cell.selectionStyle = .none
        let str = self.filterList.array?[indexPath.row].stringValue
        cell.lblName.text = str
        
        let image = str == selectedFilter ? UIImage(named: "radio_btn_selected") : UIImage(named: "radio_btn_unselected")
        cell.btnRadio.setImage(image, for: .normal)
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(btnRadioClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnRadioClicked(sender: UIButton) {
        print(sender.tag)
        let data = self.filterList.array?[sender.tag].stringValue ?? ""
        if  self.selectedFilter == data {
             self.selectedFilter = ""
        }
        else {
             self.selectedFilter = data
        }
        self.tableView.reloadData()
    }
    
    func createHeight(total : Int) {
        if total > 5 {
            self.tableView.isScrollEnabled = true
        }
        else if total == 5 {
            self.tableView.isScrollEnabled = false
        }
        else {
            self.heightTableView.constant = CGFloat(total * 35)
            self.tableView.isScrollEnabled = false
        }
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnFilterClicked(_ sender: Any) {
        self.delegate?.FilterDidFinish(data: self.selectedFilter)
        self.dismiss(animated: true, completion: nil)
    }
    
    func getFilter() {
        let kURL = "\(GlobalVariables.eventsURL)\(GetEventTypes)"
        print(kURL)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            var param = [String:String]()
            
            param["userId"] = getUserDetail("Id") == "" ? "123" : getUserDetail("Id")
            
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1" {
                        let data = json["data"]
                        self.filterList = data
                        self.createHeight(total: (self.filterList.array?.count)!)
                        self.tableView.reloadData()
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
