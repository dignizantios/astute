//
//  FirstEventTableCell.swift
//  Astute
//
//  Created by YASH on 20/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class FirstEventTableCell: UITableViewCell {

    
    @IBOutlet weak var vwTopBack: UIView!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblEventTitleOutSide: UILabel!
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDateMonthYear: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       vwTopBack.backgroundColor = UIColor.appThemeStrongRedColor
        imgEvent.layer.masksToBounds = true
        imgEvent.layer.cornerRadius = 2.0
        lblDay.textColor = UIColor.appThemeStrongRedColor
        lblMonth.textColor = UIColor.appThemeDarkYanColor
        lblEventName.textColor = UIColor.appThemeStrongRedColor
        lblEventName.font = themeFont(size: 18, fontname: .bold)
        lblMonth.font = themeFont(size: 18, fontname: .light)
        
        [lblLocation,lblTime].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkYanColor
            lbl?.font = themeFont(size: 15, fontname: .light)
        }
        
        [lblEventTitleOutSide,lblDateMonthYear].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 18, fontname: .light)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
