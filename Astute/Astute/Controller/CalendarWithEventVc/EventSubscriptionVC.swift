//
//  EventSubscriptionVC.swift
//  Pokagon Citizen Services
//
//  Created by Haresh Bhai on 26/01/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class EventSubscriptionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vcBack: UIView!
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    let arrayList = ["Street Festival 1", "Street Festival 2", "Street Festival 3", "Street Festival 4", "Street Festival 5"]
    var selectedFilter: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "EventSubscriptionCell", bundle: nil), forCellReuseIdentifier: "EventSubscriptionCell")
        tableView.tableFooterView = UIView()
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                tableView.backgroundColor = userSelectedColor
                vcBack.backgroundColor = userSelectedColor
            }
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        //vcBack.backgroundColor = UIColor.appThemeWhiteColor
        btnSubmit.backgroundColor = UIColor.appThemeStrongRedColor
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: "Event Subscription")
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_arrow_back_header") , style: .plain, target: self, action: #selector(self.btnBackClicked))
        leftButton.tintColor = .white        
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func btnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EventSubscriptionCell = self.tableView.dequeueReusableCell(withIdentifier: "EventSubscriptionCell") as! EventSubscriptionCell
        cell.selectionStyle = .none
        
        cell.btnRadio.setImage(UIImage(named: "radio_btn_unselected"), for: .normal)
        cell.lblName.text = self.arrayList[indexPath.row]
        let str = self.arrayList[indexPath.row]
        if selectedFilter.contains(str) {
            cell.btnRadio.setImage(UIImage(named: "radio_btn_selected"), for: .normal)
        }
        
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(btnRadioClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnRadioClicked(sender: UIButton) {
        print(sender.tag)
        let str = self.arrayList[sender.tag]
        if selectedFilter.contains(str) {
            for (index, event) in self.selectedFilter.enumerated() {
                if event == str {
                    selectedFilter.remove(at: index)
                }
            }
        }
        else {
            selectedFilter.append(str)
        }
        print(selectedFilter)
        self.tableView.reloadData()
    }
    
    @IBAction func btnSubimtClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
