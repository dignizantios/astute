//
//  EventCell.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblEbentNameTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var TimeView: UIView!
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!

    @IBOutlet weak var viewDay: UIView!
    
    @IBOutlet weak var imgForwordArrow: UIImageView!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgEvent.layer.masksToBounds = true
        imgEvent.layer.cornerRadius = 2.0
        
        lblEventName.textColor = UIColor.appThemeStrongRedColor
        lblEventName.font = themeFont(size: 18, fontname: .bold)
        lblMonth.textColor = UIColor.appThemeDarkYanColor
        
        [lblLocation,lblTime].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkYanColor
            lbl?.font = themeFont(size: 15, fontname: .light)
        }
        lblMonth.font = themeFont(size: 18, fontname: .light)
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
