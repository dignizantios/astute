//
//  AstuteMainVc.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage

class AstuteMainVc: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var vwUpperAstuteEvent: UIView!
    @IBOutlet weak var lblUperAstuteEvent: UILabel!
    @IBOutlet weak var lblInnerAstuteEvent: UILabel!
    @IBOutlet weak var tblEventList: UITableView!
    
    @IBOutlet weak var topBackView: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    
    @IBOutlet weak var imgHeaderBottom: UIImageView!
    @IBOutlet weak var btnProfile: CustomButton!
    
    //MARK: - Variable
    var arrayEventListing : [JSON] = []
    var arrayStaticImages : [UIImage] = []
    var dragger: TableViewDragger!

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblEventList.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
        tblEventList.tableFooterView = UIView()
        self.splashImageAndColorAPICalling()
        self.allEventListing()
        self.setupUI()
        dragger = TableViewDragger(tableView: tblEventList)
        dragger.availableHorizontalScroll = true
        dragger.dataSource = self
        dragger.delegate = self
        dragger.alphaForCell = 0.7
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.appThemeStrongRedColor
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        saveList(json: JSON.init(arrayEventListing))
    }
    
    func setupUI() {
        vwUpperAstuteEvent.isHidden = true
        arrayStaticImages.append(#imageLiteral(resourceName: "Events,.jpg"))
        arrayStaticImages.append(#imageLiteral(resourceName: "Health Campaign.png"))
        arrayStaticImages.append(#imageLiteral(resourceName: "Publications.jpg"))
        arrayStaticImages.append(#imageLiteral(resourceName: "Career Opportunities.jpg"))
        arrayStaticImages.append(UIImage(named: "ic_Word at the Day")!)
        arrayStaticImages.append(UIImage(named: "ic_phone")!)
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                tblEventList.backgroundColor = userSelectedColor
                vwBack.backgroundColor = userSelectedColor
            }
        }
        
        let imgURL = getUserDetail("Profile")
        self.btnProfile.sd_setShowActivityIndicatorView(true)
        self.btnProfile.sd_setIndicatorStyle(.gray)
        self.btnProfile.sd_setBackgroundImage(with:  URL(string: imgURL), for: .normal, placeholderImage: UIImage(named: "ic_plash_holder"), options: .lowPriority, completed: nil)
        self.btnProfile.cornerRadius = self.btnProfile.bounds.width / 2
        self.btnProfile.clipsToBounds = true
        topBackView.backgroundColor = UIColor.appThemeStrongRedColor
        vwUpperAstuteEvent.backgroundColor = UIColor.appThemeStrongRedColor
    }
}


extension AstuteMainVc : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 118 {
            vwUpperAstuteEvent.isHidden = false
        }
        else {
            vwUpperAstuteEvent.isHidden = true
        }
    }
}

extension AstuteMainVc : UITableViewDataSource,UITableViewDelegate, TableViewDraggerDataSource, TableViewDraggerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEventListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as! EventCell
        cell.viewDay.isHidden = true
        cell.imgEvent.isHidden = false
       
        let dict = self.arrayEventListing[indexPath.row]
        print(dict)
        cell.lblEventName.text = dict["Title"].stringValue
        let time = dict["Time"].stringValue
        
        cell.lblTime.text = strTostr(OrignalFormatter: "HH:mm", YouWantFormatter: "hh:mm a", strDate: time)
        
        cell.lblLocation.text = dict["Address"].stringValue
        
        cell.imgEvent.sd_setShowActivityIndicatorView(true)
        cell.imgEvent.sd_setIndicatorStyle(.gray)
        print("img",dict["Images"][0]["event_images"].url)
        print("arrayStaticImages",arrayStaticImages.count)
        if dict["Images"][0]["event_images"].url == nil{
            cell.imgEvent.image = arrayStaticImages[indexPath.row]
        }
        else {
            cell.imgEvent.sd_setImage(with: dict["Images"][0]["event_images"].url, placeholderImage: UIImage(named: "placeholder_image_listing_screen"), options: .lowPriority, completed: nil)
        }
        cell.lblEbentNameTopConstraint.constant = 25
        cell.TimeView.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrayEventListing[indexPath.row]
        print(dict)
        if dict["Title"].stringValue.lowercased() == ("Events").lowercased() {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CalednarWithEventVc") as! CalednarWithEventVc
            obj.mainIcon = arrayStaticImages[indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["Title"].stringValue.lowercased() == ("Word of the Day").lowercased()
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "WordOfTheDayVc") as! WordOfTheDayVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["Title"].stringValue.lowercased() == ("Telephone Directory").lowercased()
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "TelephoneDirectoryVc") as! TelephoneDirectoryVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["Title"].stringValue.lowercased() == ("Cultural Highlights").lowercased()
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CulturalHighlightVC") as! CulturalHighlightVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["Title"].stringValue.lowercased() == ("Upcoming Deadlines").lowercased()
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "UpcomingHighlightsVC") as! UpcomingHighlightsVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EventDetailsVC") as! EventDetailsVC
            obj.selectedController = .fromMainEventVc
            obj.dictDetails = dict
            obj.mainIcon = arrayStaticImages[indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func dragger(_ dragger: TableViewDragger, willEndDraggingAt indexPath: IndexPath) {
        saveList(json: JSON.init(arrayEventListing))
    }
    
    func dragger(_ dragger: TableViewDragger, moveDraggingAt indexPath: IndexPath, newIndexPath: IndexPath) -> Bool {
        let item = arrayEventListing[indexPath.row]
        print(item)
        arrayEventListing.remove(at: indexPath.row)
        arrayEventListing.insert(item, at: newIndexPath.row)
        tblEventList.moveRow(at: indexPath, to: newIndexPath)
        return true
    }
    
    @IBAction func btnProfileTappedAction(_ sender: UIButton)
    {
            print("User id - ",getUserDetail("Id"))
            if getUserDetail("Id") != ""
            {
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
                self.navigationController?.pushViewController(obj, animated: true)
                
            }
            else
            {
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                self.navigationController?.pushViewController(obj, animated: true)
            }
        
    }
}

//MARK: - API calling
extension AstuteMainVc {
    
    func splashImageAndColorAPICalling() {
        
        var kURL = String()
        kURL = "\(GlobalVariables.userURL)\(splashImages)"
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let param =  [ "email" : getUserDetail("EMail") == "" ? "poc" : getUserDetail("EMail")]
            print("parma \(param)")
            
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1" {
                        let data = json["data"]
//                        let backgroundColor = UIColor(red: 247.0/255.0, green: 243.0/255.0, blue: 233.0/255.0, alpha: 1)
//                        let backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
                        let backgroundColor = UIColor(red: 245.0/255.0, green: 236.0/255.0, blue: 217.0/255.0, alpha: 1)
                        let nameColor = NSKeyedArchiver.archivedData(withRootObject: backgroundColor)
                        Defaults.set(nameColor, forKey: "backgroundColor")
                        
                        self.vwBack.backgroundColor = backgroundColor
                        let splashImage = data["BackgroundImage"].stringValue
                        self.imgHeader.sd_setImage(with: URL(string: splashImage), completed: nil)
                        Defaults.set(splashImage, forKey: "splashImage")
                        Defaults.synchronize()
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    func allEventListing() {
        
        var kURL = String()
        kURL = "\(GlobalVariables.mainURL)\(mainListing)"
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let param =  ["email" : getUserDetail("EMail") == "" ? "poc" : getUserDetail("EMail")]
            print("parma \(param)")
            
            self.showLoader()
            CommonService().Service(url: kURL, param: param as [String : Any], completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1" {
                        let data = json["data"]
                        print(data)
                        self.arrayEventListing = data.arrayValue
                        //Add Static Data for Word Of Day and Telephone Disctionary
                        print("arrayEventListing \(self.arrayEventListing)")
                        
                        /*var staticData = JSON()
                        staticData["Eventid"].stringValue = "31"
                        staticData["Title"].stringValue = "Word at the Day"
                        staticData["Time"].stringValue = "15:28"
                        staticData["EventType"].stringValue = "Wellness"
                        staticData["Description"].stringValue = "Pokagon Health Services launched a campaign in January 2017 that will restart every January to inspire and empower our people to healthier lifestyles. The goal is to get as many citizens, PHS patients, and staff from the tribal government to improve their health through four main areas: physical, emotional, rest, and nutrition. Stop by the Wellness Center during regular hours to get measured and join the campaign! \n By working with the various teams in PHS through this one year campaign, those involved will see improvement in their health. We hope you participate and let us help you choose a good path for your health.\n Participants will track progress with quarterly measurements.Head to your nearby measurement station once during each of the below time frames.This will help you keep on track all the way to December 2018."
                        staticData["Time"].stringValue = "15:28"
                        staticData["SignUpRequired"].stringValue = "false"
                        staticData["Eventid"].stringValue = "75"
                        staticData["Address"].stringValue = "Human Resource Dept"
                        self.arrayEventListing.append(staticData)
                        
                         staticData = JSON()
                        staticData["Title"].stringValue = "Telephone Directory"
                        staticData["Eventid"].stringValue = "31"
                        staticData["Time"].stringValue = "15:28"
                        staticData["EventType"].stringValue = "Wellness"
                        staticData["Description"].stringValue = "Pokagon Health Services launched a campaign in January 2017 that will restart every January to inspire and empower our people to healthier lifestyles. The goal is to get as many citizens, PHS patients, and staff from the tribal government to improve their health through four main areas: physical, emotional, rest, and nutrition. Stop by the Wellness Center during regular hours to get measured and join the campaign! \n By working with the various teams in PHS through this one year campaign, those involved will see improvement in their health. We hope you participate and let us help you choose a good path for your health.\n Participants will track progress with quarterly measurements.Head to your nearby measurement station once during each of the below time frames.This will help you keep on track all the way to December 2018."
                        staticData["Time"].stringValue = "15:30"
                        staticData["SignUpRequired"].stringValue = "false"
                        staticData["Eventid"].stringValue = "75"
                        staticData["Address"].stringValue = "Human Resource Dept"
                        //data.
                        self.arrayEventListing.append(staticData)
                        
                        print("arrayEventListing \(self.arrayEventListing)")*/
                        
                        if self.loadJSON().isEmpty {
                            self.saveList(json: JSON(self.arrayEventListing))
                        }
                        else {
                            let savedJson = self.loadJSON().arrayValue
                            if data.arrayValue.count == self.loadJSON().arrayValue.count {
                                self.arrayEventListing = self.loadJSON().arrayValue
                            }
                            else {
                                self.arrayEventListing = self.loadJSON().arrayValue
                                for i in 0..<data.arrayValue.count {
                                    let json = data.arrayValue[i]
                                    if savedJson.contains(json) {
                                        self.arrayEventListing.append(json)
                                    }
                                }
                                self.saveList(json: JSON.init(self.arrayEventListing))
                            }
                        }
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                self.tblEventList.reloadData()
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    func saveList(json: JSON) {
        Defaults.removeObject(forKey: "mainListing")
        let jsonString = json.rawString()!
        Defaults.setValue(jsonString, forKey: "mainListing")
        Defaults.synchronize()
    }
    
    public func loadJSON() -> JSON {
        return JSON.init(parseJSON: (Defaults.value(forKey: "mainListing") as? String ?? ""))
        // JSON from string must be initialized using .parse()
    }
    
}
