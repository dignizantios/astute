//
//  GuestDetailsVc.swift
//  Astute
//
//  Created by YASH on 21/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown

class GuestDetailsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblGuestDetails: UITableView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet var vwFooter: UIView!
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var lblGuestName: UILabel!
    @IBOutlet weak var txtGuestName: CustomTextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var lblRelationShip: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var txtAge: CustomTextField!
    @IBOutlet weak var lblTshirt: UILabel!
    @IBOutlet weak var txtTshirt: CustomTextField!
    @IBOutlet weak var btnAddMore: CustomButton!
    @IBOutlet var btnRadio: [UIButton]! // tag start from 0 to 5
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - Variable
    
    var selectedRelationShip = ""
    
    var arrayAddDetails : [JSON] = []
    var finalAddGuestDetailsParam : [JSON] = []
    let tshirtSizeDD = DropDown()
    
    var arrayTshirt : [String] = []
    
    
    var dictDetails = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblGuestDetails.register(UINib(nibName: "GuestDetailsCell", bundle: nil), forCellReuseIdentifier: "GuestDetailsCell")
        tblGuestDetails.tableFooterView = UIView()
        
        tblGuestDetails.tableHeaderView = vwHeader
        tblGuestDetails.tableFooterView = vwFooter
        
        self.setupUI()
        
        arrayTshirt = ["S","M","XL","XXL","XXXL"]
        
        self.tshirtSizeDD.dataSource = self.arrayTshirt
        
        NotificationCenter.default.addObserver(self, selector: #selector(addDetailsToArray), name: NSNotification.Name(rawValue: "addDetails"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Enter_Guest_Details_key"))
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.tshirtSizeDD, sender: self.txtTshirt)
        selectionIndex()
    }
    
    func setupUI()
    {
        
        let sortedArray = btnRadio.sorted { (btn1, btn2) -> Bool in
            return btn1.tag < btn2.tag
        }
        btnRadio = sortedArray
        
        [txtGuestName,txtEmail,txtAge,txtTshirt].forEach { (txt) in
            txt?.placeholder = getCommonString(key: "Enter_here_key")
            txt?.placeHolderColor = UIColor.appThemeBlackColor
            txt?.leftPaddingView = 15
            txt?.cornerRadius = 3
            txt?.backgroundColor = UIColor.appThemeLightPurpleColor
            txt?.layer.borderColor = UIColor.appThemeStrongRedColor.cgColor
            txt?.layer.borderWidth = 0.5
            txt?.font = themeFont(size: 15, fontname: .light)
            txt?.delegate = self
        }
        
        addDoneButtonOnKeyboard(textfield: txtAge)
        
        txtTshirt.placeholder = getCommonString(key: "Select_here_key")
        txtTshirt.placeHolderColor = UIColor.appThemeBlackColor
        
        addDoneButtonOnKeyboard(textfield: txtAge)
        
        [lblGuestName,lblEmail,lblAge,lblTshirt,lblRelationShip].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeStrongRedColor
            
            lbl?.font = themeFont(size: 18, fontname: .light)
        }
        
        lblGuestName.text = getCommonString(key: "Guest_Name_key")
        lblEmail.text = getCommonString(key: "Email_key")
        lblRelationShip.text = getCommonString(key: "Realtionship_key")
        lblAge.text = getCommonString(key: "Age_key")
        lblTshirt.text = getCommonString(key: "T-Shirt_size_key")
        
        btnAddMore.setTitle(getCommonString(key: "ADD_MORE_key"), for: .normal)
        btnAddMore.setTitleColor(UIColor.appThemeStrongRedColor, for: .normal)
        
        btnRadio[0].setTitle(getCommonString(key: "Self_key"), for: .normal)
        btnRadio[1].setTitle(getCommonString(key: "Parent_key"), for: .normal)
        btnRadio[2].setTitle(getCommonString(key: "Guardian_key"), for: .normal)
        btnRadio[3].setTitle(getCommonString(key: "Child_key"), for: .normal)
        btnRadio[4].setTitle(getCommonString(key: "Employee_key"), for: .normal)
        btnRadio[5].setTitle(getCommonString(key: "Spouse_key"), for: .normal)
        
        btnRadio.forEach { (btn) in
            btn.setTitleColor(UIColor.appThemeDarkYanColor, for: .normal)
            btn.titleLabel?.font = themeFont(size: 15, fontname: .light)
        }
        
        btnSubmit.setTitle(getCommonString(key: "SUBMIT_key"), for: .normal)
        btnSubmit.setupThemeButtonUI()
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
                vwBack.backgroundColor = userSelectedColor
            }
        }
        
    }
    
}


//MARK: - DropDown Setup

extension GuestDetailsVc
{
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .bottom
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    
    
    func selectionIndex()
    {
        
        self.tshirtSizeDD.selectionAction = { (index, item) in
            self.txtTshirt.text = item
            self.view.endEditing(true)
            self.txtTshirt.text = self.arrayTshirt[index]
            
            self.tshirtSizeDD.hide()
        }
        
    }
}

//MARK: - IBAction method

extension GuestDetailsVc
{
    
    @IBAction func btnAddMoreTapped(_ sender: UIButton) {
        
        var dict = JSON()
        dict["name"].stringValue = ""
        dict["age"].stringValue = ""
        dict["T-shirtsize"].stringValue = ""
        
        self.arrayAddDetails.append(dict)
        
        self.tblGuestDetails.reloadData()
        
        self.tblGuestDetails.scrollToRow(at: IndexPath(row: self.arrayAddDetails.count - 1, section: 0), at: .bottom, animated: true)
        
    }
    
    
    @IBAction func btnRadioTapped(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
        
        for i in 0..<btnRadio.count
        {
            if sender.tag == i
            {
                btnRadio[i].isSelected = true
                self.selectedRelationShip = "\(i)"
            }
            else
            {
                btnRadio[i].isSelected = false
            }
        }
        
        print("selectedRelationShip : \(self.selectedRelationShip)")
        
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
       
        if (txtGuestName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_guest_name_key"))
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email_key"))
        }
        else if selectedRelationShip == ""
        {
            makeToast(strMessage: getCommonString(key: "Please_select_relationship_key"))
        }
        else if (txtAge.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_age_key"))
        }
        else if (txtTshirt.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_select_T-shirt_size_key"))
        }
        else if self.arrayAddDetails.contains(where: { (json) -> Bool in
            
            if(json["name"].stringValue == "" || json["age"].stringValue == "" || json["T-shirtsize"].stringValue == "")
            {
                return true
            }
            return false
            
        }){
            
            makeToast(strMessage: getCommonString(key: "Please_enter_all_details_of_guest_key"))
            
        }else{
            
            self.finalAddGuestDetailsParam = self.arrayAddDetails
            
            var dict = JSON()
            dict["name"].stringValue = txtGuestName.text!
            dict["email"].stringValue = txtEmail.text!
            dict["relationship"].stringValue = self.selectedRelationShip
            dict["age"].stringValue = txtAge.text!
            dict["T-shirtsize"].stringValue = txtTshirt.text!
            
            self.finalAddGuestDetailsParam.append(dict)
            
            print("self.finalAddParam : \(self.finalAddGuestDetailsParam)")
            
            let strAllGuest = arrayToJSONString(arr: self.finalAddGuestDetailsParam)
            
            self.submitAllGuestDetails(strGuestData: strAllGuest)
            
            
        }
        
    }
    
    
    @objc func removeGuest(_ sender : UIButton)
    {
        self.arrayAddDetails.remove(at: sender.tag)
        self.tblGuestDetails.reloadData()
    }
    
    @objc func addDetailsToArray(noti : NSNotification)
    {
        
        if let data = noti.object as? JSON
        {
//            print("data : \(data)")
            
            let index = data["index"].intValue
            
            var dict = self.arrayAddDetails[index]
            dict["name"].stringValue = data["name"].stringValue
            dict["age"].stringValue = data["age"].stringValue
            dict["T-shirtsize"].stringValue = data["T-shirtsize"].stringValue
            
            self.arrayAddDetails[index] = dict
            
        }
        
    }
    
    func arrayToJSONString(arr : [JSON]) -> String
    {
        let theJSONText = JSON(arr).rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        return theJSONText
        
    }
    
}





//MARK: - TextField delegate

extension GuestDetailsVc : UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        if textField == txtTshirt
        {
            tshirtSizeDD.show()
            return false
        }
        
        return true
        
    }
    
}


//MARK: - TableView data Source
extension GuestDetailsVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayAddDetails.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestDetailsCell") as! GuestDetailsCell
        
        cell.tag = indexPath.row
        cell.btnClose.tag = indexPath.row
        
        cell.btnClose.addTarget(self, action: #selector(removeGuest), for: .touchUpInside)
        
        var dict = self.arrayAddDetails[indexPath.row]
        
        cell.txtAdditionalGuestName.text = dict["name"].stringValue
        cell.txtAge.text = dict["age"].stringValue
        cell.txtTshirtSize.text = dict["T-shirtsize"].stringValue
        
        cell.configOptionsDD(sender:cell.txtTshirtSize)
        cell.tshirtGuestDD.dataSource = self.arrayTshirt
        
        return cell
        
    }
    
}

//MARK: - APi calling

extension GuestDetailsVc
{
    
    func submitAllGuestDetails(strGuestData : String)
    {
        
        var kURL = String()
        kURL = "\(GlobalVariables.userURL)\(submitGuestDetails)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [
                "userId" : getUserDetail("Id") == "" ? "123" : getUserDetail("Id"),
                "eventId" : dictDetails["Eventid"].stringValue,
                "guest" : strGuestData
                ]
            
            
            print("parma \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1"
                    {
                        let data = json["data"]
                        makeToast(strMessage: json["Message"].stringValue)
                       
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
}
