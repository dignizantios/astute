//
//  GuestDetailsCell.swift
//  Astute
//
//  Created by YASH on 21/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown

class GuestDetailsCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblAdditionalGuestName: UILabel!
    @IBOutlet weak var txtAdditionalGuestName: CustomTextField!
    
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var txtAge: CustomTextField!
    
    @IBOutlet weak var lblTshirtSize: UILabel!
    @IBOutlet weak var txtTshirtSize: CustomTextField!
    
    @IBOutlet weak var btnClose: UIButton!
    
    var name = ""
    var age = ""
    var tshirt = ""
    
    
    var tshirtGuestDD = DropDown()
    
    var addedDetailsJSON = JSON()
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [txtAdditionalGuestName,txtAge,txtTshirtSize].forEach { (txt) in
            txt?.placeholder = getCommonString(key: "Enter_here_key")
            txt?.placeHolderColor = UIColor.appThemeBlackColor
            txt?.leftPaddingView = 15
            txt?.cornerRadius = 3
            txt?.backgroundColor = UIColor.appThemeLightPurpleColor
            txt?.layer.borderColor = UIColor.appThemeStrongRedColor.cgColor
            txt?.layer.borderWidth = 0.5
            txt?.font = themeFont(size: 15, fontname: .light)
            txt?.delegate = self
        }
        
        txtTshirtSize.placeholder = getCommonString(key: "Select_here_key")
        txtTshirtSize.placeHolderColor = UIColor.appThemeBlackColor
        
        [lblAdditionalGuestName,lblAge,lblTshirtSize].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeStrongRedColor
            lbl?.font = themeFont(size: 15, fontname: .light)
        }

        addDoneButtonOnKeyboard(textfield: txtAge)
        
        txtAdditionalGuestName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        txtAge.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        selectionIndex()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GuestDetailsCell
{
    
    func configOptionsDD(sender: UITextField)
    {
        tshirtGuestDD.anchorView = sender
        tshirtGuestDD.direction = .any
        tshirtGuestDD.dismissMode = .onTap
        //dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        tshirtGuestDD.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        tshirtGuestDD.width = sender.bounds.width
        tshirtGuestDD.cellHeight = 40.0
        tshirtGuestDD.backgroundColor = UIColor.white
        
    }
    
    
    func selectionIndex()
    {
        
        self.tshirtGuestDD.selectionAction = { (index, item) in
            self.txtTshirtSize.text = item
//            self.tshirtGuestDD.next = self.arrayTshirt[index]

            self.contentView.endEditing(true)
            self.addedDetailsJSON["index"].intValue = self.tag
            self.addedDetailsJSON["T-shirtsize"].stringValue = item
            self.tshirtGuestDD.hide()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addDetails"), object: self.addedDetailsJSON)
            
        }
        
    }
    
}

extension GuestDetailsCell : UITextFieldDelegate
{
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        
        if textField == txtAdditionalGuestName
        {
            name = txtAdditionalGuestName.text?.trimmingCharacters(in: .whitespaces) ?? ""
        }
        else if textField == txtAge
        {
            age = txtAge.text?.trimmingCharacters(in: .whitespaces) ?? ""
        }
        else if textField == txtTshirtSize
        {
            tshirt = txtTshirtSize.text?.trimmingCharacters(in: .whitespaces) ?? ""
        }
        
        addedDetailsJSON["name"].stringValue = name
        addedDetailsJSON["age"].stringValue = age
//        dict["T-shirtsize"].stringValue = tshirt
        addedDetailsJSON["index"].intValue = self.tag
        
    //    print("Dict : \(addedDetailsJSON)")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addDetails"), object: addedDetailsJSON)
        
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtTshirtSize
        {
            self.contentView.endEditing(true)
            tshirtGuestDD.show()
            return false
        }
        
        return true
    }
}
