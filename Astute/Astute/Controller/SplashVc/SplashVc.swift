//
//  SplashVc.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SDWebImage

class SplashVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var imgSplash: UIImageView!
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //printFonts()
        if let images = Defaults.value(forKey: "splashImage") as? String
        {
            let imgUrl = URL(string: images)
            
            imgSplash.sd_setShowActivityIndicatorView(true)
            imgSplash.sd_setIndicatorStyle(.gray)
            imgSplash.sd_setImage(with: imgUrl, placeholderImage: nil, options: .lowPriority, completed: nil)
            
        }
        
        self.perform(#selector(redirectToLogin), with: nil, afterDelay: 3.0)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
}


extension SplashVc
{
    
    //MARK: - API Calling
    
    @objc func redirectToLogin()
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AstuteMainVc") as! AstuteMainVc
        
        self.navigationController?.pushViewController(obj, animated: true)
        /*
        print("User id - ",getUserDetail("Id"))
        if getUserDetail("Id") != ""
        {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AstuteMainVc") as! AstuteMainVc
            
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
            self.navigationController?.pushViewController(obj, animated: true)
        }*/
        
    }
    
}
