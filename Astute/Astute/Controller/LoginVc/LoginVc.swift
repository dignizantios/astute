//
//  LoginVc.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import LinkedinSwift
import GoogleSignIn

class LoginVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var VwLogin: CustomView!
    @IBOutlet weak var lblLoginWith: UILabel!
    @IBOutlet weak var btnFB: CustomButton!
    @IBOutlet weak var btnLinkdIn: CustomButton!
    @IBOutlet weak var btnGoogle: CustomButton!
    @IBOutlet weak var txtCitizenId: CustomTextField!
    @IBOutlet weak var VwLastName: CustomView!
    @IBOutlet weak var txtLastName: CustomTextField!
    @IBOutlet weak var txtDOB: CustomTextField!
    
    @IBOutlet weak var VwDOB: CustomView!
    //MARK: - Variable
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: Linked_In.LINKEDIN_CLIENT_ID, clientSecret: Linked_In.LINKEDIN_CLIENT_SECRET, state: Linked_In.LINKEDIN_CLIENT_STATE, permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: Linked_In.LINKEDIN_REDIRECT_URI))
    
    let datePicker = UIDatePicker()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.setupUI()
        //        GIDSignIn.sharedInstance().delegate = self
        self.createDatePicker()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        [VwLogin,VwLastName,VwDOB].forEach { (vw) in
            vw?.backgroundColor = UIColor.appThemeLightPurpleColor
            
        }
        [txtCitizenId,txtLastName,txtDOB].forEach { (txt) in
            txt?.placeHolderColor = UIColor.appThemeBlackColor
            txt?.font = themeFont(size: 14, fontname: .light)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Login_Key"))
        
//        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupUI() {
        
        vwBack.backgroundColor = UIColor.appThemeStrongRedColor
        lblLoginWith.textColor = UIColor.appThemeLightGrayColor
        lblLoginWith.font = themeFont(size: 13, fontname: .light)
        lblLoginWith.text = getCommonString(key: "LOGIN_WITH_key")
        
        btnFB.backgroundColor = UIColor(red: 43.0/255.0, green: 85.0/255.0, blue: 157.0/255.0, alpha: 1.0)
        btnLinkdIn.backgroundColor = UIColor(red: 0.0/255.0, green: 132.0/255.0, blue: 177.0/255.0, alpha: 1.0)
        btnGoogle.backgroundColor = UIColor(red: 221.0/255.0, green: 75.0/255.0, blue: 57.0/255.0, alpha: 1.0)
        
        [btnFB,btnLinkdIn,btnGoogle].forEach { (btn) in
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
        }
        
        
        btnFB.setTitle(getCommonString(key: "FACEBOOK_key"), for: .normal)
        btnLinkdIn.setTitle(getCommonString(key: "LINKDIN_key"), for: .normal)
        btnGoogle.setTitle(getCommonString(key: "GOOGLE_key"), for: .normal)
    }
    
    func createDatePicker(){
        //assign datepicker to our textfield
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        self.txtDOB.inputView = datePicker
        
        //create a toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //add a done button on this toolbar
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        toolbar.setItems([doneButton], animated: true)
        self.txtDOB.inputAccessoryView = toolbar
    }
    
    @objc func doneClicked() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "MM-dd-yyyy"
        self.txtDOB.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
}


//MARK: - IBAction

extension LoginVc {
    
    @IBAction func btnFBTappedAction(_ sender: UIButton) {
        GlobalVariables.socialLogin = "0"
        fbLoginAction()
    }
    
    @IBAction func btnLinkedinTappedAction(_ sender: UIButton) {
        GlobalVariables.socialLogin = "1"
        loginWithLinked()
    }
    
    @IBAction func btnGoogleTappedAction(_ sender: UIButton) {
        GlobalVariables.socialLogin = "2"
        loginWithGoogle()
    }
    
    @IBAction func btnLoginTappedAction(_ sender: UIButton) {
        CreateValidation()        
    }
    
}

//MARK: - FBLogin
extension LoginVc {
    
    //MARK: - FB Method
    func fbLoginAction() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: { (result, error) -> Void in
            if (error == nil) {
                print(result!)
                if (result?.grantedPermissions != nil) {
                    self.showLoader()
                    self.returnUserData()
                }
                else {
                    self.hideLoader()
                }
            }
            else {
                self.hideLoader()
            }
        })
    }
    
    func returnUserData() {
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    print("Result : \(JSON(result))")
                    
                    self.callSocialLoginWebservice(dict: JSON(result as! NSDictionary))
                }
                else {
                    print("Errrorr : \(error)")
                }
            })
        }
    }
    
    func callSocialLoginWebservice(dict:JSON) {
        
        var kURL = String()
        kURL = "\(GlobalVariables.userURL)\(register)"
        
        print("URL:- \(kURL)")
        
        var imgProfile = ""
        
        if dict["picture"]["data"]["url"].exists() {
            imgProfile = dict["picture"]["data"]["url"].stringValue
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            var param = [String:String]()
            
            if GlobalVariables.socialLogin == "0" {
                param["email"] = dict["email"].stringValue
                param["mobile"] = ""
                param["name"] = dict["name"].stringValue
                param["profile"] = imgProfile
            }
            else if GlobalVariables.socialLogin == "1" {
                param["email"] = dict["emailAddress"].stringValue
                param["mobile"] = ""
                param["name"] = "\(dict["firstName"].stringValue) \(dict["lastName"].stringValue)"
                param["profile"] = dict["pictureUrl"].stringValue
                
            }
            else if GlobalVariables.socialLogin == "2" {
                param["email"] = dict["email"].stringValue
                param["mobile"] = ""
                param["name"] = dict["name"].stringValue
                param["profile"] = dict["url"].stringValue
            }
            
            print("parma \(param)")
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1" {
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AstuteMainVc") as! AstuteMainVc
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    func CreateValidation() {
        if self.txtCitizenId.text! == "" {
            makeToast(strMessage:  "Please enter citizen id")
        }
        else if self.txtLastName.text! == "" {
            makeToast(strMessage:  "Please enter Last name")
        }
        else {
            self.CreateLogin()
        }
    }
    
    func CreateLogin() {
        let kURL = "\(GlobalVariables.userURL)\(Login)"
        print(kURL)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            var param = [String:String]()
            
            param["tribalId"] = self.txtCitizenId.text!
            param["lastName"] = self.txtLastName.text!
            param["dob"] = self.txtDOB.text!
            
            print("parma \(param)")
            self.showLoader()
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                self.hideLoader()
                if let json = respones.value {
                    print("json \(json)")
                    if json["Flag"].stringValue == "1" {
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AstuteMainVc") as! AstuteMainVc
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else {
                        makeToast(strMessage: json["Message"].stringValue)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
}


//MARK: - LinkedIn
extension LoginVc {
    
    func loginWithLinked() {
        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
            self.requestProfile()
            // self.writeConsoleLine("Login success lsToken: \(lsToken)")
            }, error: { [unowned self] (error) -> Void in
                
                self.writeConsoleLine("Encounter error: \(error.localizedDescription)")
            }, cancel: { [unowned self] () -> Void in
                
                self.writeConsoleLine("User Cancelled!")
        })
    }
    
    @IBAction func requestProfile() {
        linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
            print("response \(JSON(response.jsonObject))")
            self.callSocialLoginWebservice(dict: JSON(response.jsonObject))
        }) { [unowned self] (error) -> Void in
            self.writeConsoleLine("Encounter error: \(error.localizedDescription)")
        }
    }
    
    fileprivate func writeConsoleLine(_ log: String, funcName: String = #function) {
        print("log \(log)")
        print("funcName \(funcName)")
    }
}

//MARK: - Google login
extension LoginVc {
    
    private func loginWithGoogle() {
        GIDSignIn.sharedInstance().clientID   = "536031152605-i1p6hd2va8dhlmapj596hpr5j6furcfj.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate   = self
        GIDSignIn.sharedInstance().uiDelegate = self
        if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            logInToBackendServerAuthIdToken()
            GIDSignIn.sharedInstance().signOut()
        }
        GIDSignIn.sharedInstance().signIn()
    }
    func logInToBackendServerAuthIdToken(){
        let user = GIDSignIn.sharedInstance().currentUser
        print(user) // fatal error: unexpectedly found nil
    }
}


extension LoginVc: GIDSignInUIDelegate,GIDSignInDelegate {
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    //when the signin complets
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        //if any error stop and print the error
        if error != nil{
            print(error ?? "google error")
            return
        }else{
            // Perform any operations on signed in user here.
            //if success display the email on label
            var imageURL = ""
            if user.profile.hasImage {
                imageURL  = user.profile.imageURL(withDimension: 120).absoluteString
                print("imageURL\(imageURL)")
            }
            
            let dic = ["name":user.profile.name,"gender":"","birthday":"","url":imageURL, "email" : user.profile.email ,"id":user.authentication.idToken ] as NSDictionary
            print("dic:=\(JSON(dic))")
            
            self.callSocialLoginWebservice(dict: JSON(dic))
        }
    }
}
