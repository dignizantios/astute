//
//  EventDetailsVC.swift
//  Astute
//
//  Created by YASH on 20/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import Foundation
import EventKit

enum checkParentController
{
    case fromMainEventVc
    case fromCalendarEventVc
}



class EventDetailsVC: UIViewController ,UIDocumentInteractionControllerDelegate{

    //MARK: - Outlet
    
    @IBOutlet weak var collectionImgs: UICollectionView!
    
    @IBOutlet weak var conllectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var lblEventTitle: UILabel!
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var vwDate: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnLeft: UIButton!
    
    @IBOutlet weak var btnEnterGuestDetails: CustomButton!
    
    @IBOutlet weak var btnAddCalendar: CustomButton!
    @IBOutlet weak var bottomSignUpView: UIView!
    @IBOutlet weak var SignUpButtonHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Variable
    
    var dictDetails = JSON()
    var arrayImages : [JSON] = []
    var selectedController = checkParentController.fromMainEventVc
    var stringDate = Date()
    var mainIcon = UIImage()
    
    let eventStore : EKEventStore = EKEventStore()

    var arrHeaderImage:[JSON] = []
    {
        didSet {
            if selectdImg == 0
            {
                self.btnLeft.isHidden = true
            }else{
                [btnLeft,btnRight].forEach { $0?.isHidden = arrayImages.count < 2 }
            }
            
        }
    }
    var selectdImg = 0{
        didSet {
            btnLeft.isHidden = selectdImg == 0
            if arrayImages.count != 0
            {
                btnRight.isHidden = selectdImg == (arrayImages.count - 1)
            }
            else
            {
                btnRight.isHidden = true
            }
            
        }
    }
    var isCulturalHighlightDetails = Bool()
    var culturalId = String()
    
    var isUpcomingDeadline = Bool()
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionImgs.register(UINib(nibName: "ImgCollectionCell", bundle: nil), forCellWithReuseIdentifier:"ImgCollectionCell")
        
        
        // Do any additional setup after loading the view.
        if isCulturalHighlightDetails{
            setupCultureHighlights()
        }else{
            self.setupUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    
    func setupUI()
    {
        
        [lblDate,lblTime,lblDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 16, fontname: .light)
        }
        
        lblLocation.textColor = UIColor.appThemeDarkYanColor
        lblLocation.font = themeFont(size: 16, fontname: .light)
        
        conllectionViewHeightConstraint.constant = 0
        collectionImgs.isHidden = true
        
        lblEventTitle.textColor = UIColor.appThemeStrongRedColor
        lblEventTitle.font = themeFont(size: 20, fontname: .light)
        if isUpcomingDeadline{
            locationView.isHidden = true
            timeView.isHidden = true
            self.arrayImages = []
            bottomSignUpView.isHidden = true
            SignUpButtonHeightConstraint.constant = 0
            setupNavigationbarwithBackButton(titleText: getCommonString(key: "Upcoming_Deadlines_key"))
             lblEventTitle.font = themeFont(size: 20, fontname: .light)
             lblEventTitle.text = dictDetails["title"].stringValue
            lblDate.textColor = UIColor.appThemeDarkYanColor
             lblDate.text = dictDetails["deadline_date"].stringValue
             //lblDescription.text = dictDetails["description"].stringValue
            let string = dictDetails["description"].stringValue
            let attributedString = NSMutableAttributedString(string: string, attributes:[NSAttributedString.Key.link: URL(string: "\(dictDetails["link"].stringValue)")!])
            lblDescription.attributedText = attributedString
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onClicLabel(sender:)))
            lblDescription.isUserInteractionEnabled = true
            lblDescription.addGestureRecognizer(tap)
        }else{
          
            btnEnterGuestDetails.setTitle(getCommonString(key: "ENTER_GUEST_DETAILS_key"), for: .normal)
            btnEnterGuestDetails.setupThemeButtonUI()
            
            btnAddCalendar.setTitle("ADD TO YOUR CALENDAR", for: .normal)
            btnAddCalendar.setupThemeButtonUI()
             print("DictDetails : \(dictDetails)")
            if selectedController == .fromMainEventVc
            {
                // getEventDetails(id: dictDetails["Eventid"].stringValue)
                 setupNavigationbarwithBackButton(titleText: dictDetails["Title"].stringValue)
                lblEventTitle.font = themeFont(size: 20, fontname: .light)
                lblEventTitle.text = dictDetails["Title"].stringValue
                lblDescription.text = dictDetails["Description"].stringValue
                lblLocation.text = dictDetails["Address"].stringValue
                lblTime.textColor = UIColor.appThemeDarkYanColor
                 let time = dictDetails["Time"].stringValue
                 lblTime.text = strTostr(OrignalFormatter: "HH:mm", YouWantFormatter: "hh:mm a", strDate: time)
                 vwDate.isHidden = true
                if dictDetails["SignUpRequired"].boolValue == true{
                    bottomSignUpView.isHidden = false
                    btnAddCalendar.isHidden = true
                    SignUpButtonHeightConstraint.constant = 60
                }else{
                    bottomSignUpView.isHidden = true
                    SignUpButtonHeightConstraint.constant = 0
                }
                
            }else{
                 setupNavigationbarwithBackButton(titleText: dictDetails["title"].stringValue)
                getEventDetails(id: dictDetails["event_id"].stringValue)
                vwDate.isHidden = false
                btnAddCalendar.isHidden = false
                lblEventTitle.font = themeFont(size: 20, fontname: .light)
                lblTime.textColor = UIColor.appThemeDarkYanColor
                lblDate.textColor = UIColor.appThemeDarkYanColor
                let value = DateToString(Formatter: "dd-MMMM-yyyy", date: stringDate)
                lblDate.text = value
            }
            
            
             //getGetICSCalandarEvent(id:dictDetails["event_id"].stringValue)
            self.arrayImages = []
            
            if dictDetails["Images"].stringValue != "null"
            {
                
                if dictDetails["Images"].arrayValue.count > 0
                {
                    self.arrayImages = dictDetails["Images"].arrayValue
                }
                
            }
            
            //        if arrayImages.count == 0 || arrayImages.count == 1
            //        {
            //            btnLeft.isHidden = true
            //            btnRight.isHidden = true
            //
            //        }
            //        else
            //        {
            //            btnLeft.isHidden = true
            //            btnRight.isHidden = false
            //        }
            btnLeft.isHidden = true
            btnRight.isHidden = true
        }
        
        
    }
    
    func setupCultureHighlights(){
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Cultural_Highlights_key"))
        [lblLocation,lblDate,lblTime,lblDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 16, fontname: .light)
        }
        lblEventTitle.textColor = UIColor.appThemeStrongRedColor
        lblEventTitle.font = themeFont(size: 20, fontname: .light)
        locationView.isHidden = true
        vwDate.isHidden = true
        timeView.isHidden = true
        self.arrayImages = []
        bottomSignUpView.isHidden = true
        SignUpButtonHeightConstraint.constant = 0
        getCulturalHighlightDetails(id: culturalId)
        
        
    }
    
    // And that's the function :)
    @objc func onClicLabel(sender:UITapGestureRecognizer) {
        openUrl(urlString: "\(dictDetails["link"].stringValue)")
    }
    
    
    func openUrl(urlString:String!) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }

}


//MARK:- Scrollview Delegate

extension EventDetailsVC:UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionImgs
        {
            let count = scrollView.contentOffset.x / scrollView.frame.size.width
            selectdImg = Int(count)
        }
    }
}

//MARK: - IBAction

extension EventDetailsVC
{
    
    @IBAction func btnEnterGuestDetailsTappedAction(_ sender: UIButton)
    {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "GuestDetailsVc")  as! GuestDetailsVc
        obj.dictDetails = self.dictDetails
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnAddToCalendarTappedAction(_ sender: UIButton)
    {

        
        /*
        var path: URL? = Bundle.main.url(forResource: "15210", withExtension: "ics")
        if let path = path {
            UIApplication.shared.openURL(path)
        }*/
        
        eventStore.requestAccess(to: .event) { (granted, error) in
            
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error \(String(describing: error))")
                let aURL = URL(string: "\(GlobalVariables.webApiURL)\(GetICSCalandarEvent)\(self.dictDetails["id"].stringValue)")
                print("\("\(GlobalVariables.webApiURL)\(GetICSCalandarEvent)\(self.dictDetails["id"].stringValue)")" as Any)
                DispatchQueue.main.async {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(aURL!, options: [:])
                    } else {
                        UIApplication.shared.openURL(aURL!)
                    }
                }
                
               /* DispatchQueue.main.async {
                    let path = Bundle.main.path(forResource: "16414", ofType: "ics")
                    let url = URL(fileURLWithPath: path ?? "")
                    let dc = UIDocumentInteractionController(url: url)
                    dc.delegate = self
                    dc.presentPreview(animated: true)
                }*/
                
                /*
                let event:EKEvent = EKEvent(eventStore: self.eventStore)
                
                
                event.title = self.dictDetails["Title"].stringValue
                event.startDate = self.stringDate
                event.endDate = self.stringDate
                event.notes = self.dictDetails["Description"].stringValue
                event.calendar = self.eventStore.defaultCalendarForNewEvents
                do {
                    try self.eventStore.save(event, span: .thisEvent)
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                makeToast(strMessage: "Event has been saved.")*/
            }
            else{
                
                print("failed to save event with error : \(String(describing: error)) or access not granted")
            }
        }
    }
    
    @IBAction func btnRightImgTapped(_ sender: UIButton) {
        self.btnRight.isHidden = false
        print("selected \(selectdImg)")
        print("arrayImages Count \(arrayImages.count-1)")
        
        if selectdImg < arrayImages.count-1
        {
            selectdImg += 1
            self.collectionImgs.scrollToItem(at: IndexPath(item: selectdImg, section: 0), at: .centeredHorizontally, animated: true)
            
            return
        }
        self.btnRight.isHidden = true
        
    }
    
    @IBAction func btnLeftImgTapped(_ sender: UIButton) {
        self.btnRight.isHidden = false
        if selectdImg == 0
        {
            self.btnRight.isHidden = true
            return
        }
        selectdImg -= 1
        self.collectionImgs.scrollToItem(at: IndexPath(item: selectdImg, section: 0), at: .centeredHorizontally, animated: true)
    }
    
}


//MARK: - CollectionView Delegate and DataSource

extension EventDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if arrayImages.count > 0
        {
            return arrayImages.count
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImgCollectionCell", for: indexPath) as! ImgCollectionCell
    
        selectdImg = indexPath.row
        if isCulturalHighlightDetails{
            cell.img.sd_setShowActivityIndicatorView(true)
            cell.img.sd_setIndicatorStyle(.gray)
            print("src-->\(arrayImages[indexPath.row]["src"].stringValue)")
            if arrayImages[indexPath.row]["src"].stringValue == nil{
                cell.img.image = mainIcon//UIImage(named: "placeholder_image_banner")
            }else{
                cell.img.sd_setImage(with: arrayImages[indexPath.row]["src"].url, placeholderImage: UIImage(named: "placeholder_image_banner"), options: .lowPriority, completed: nil)
            }
        }else{
            if arrayImages.count > 0
            {
                let dict = arrayImages[indexPath.row]
                cell.img.sd_setShowActivityIndicatorView(true)
                cell.img.sd_setIndicatorStyle(.gray)
                if dict["event_images"].url == nil{
                    cell.img.image = mainIcon//UIImage(named: "placeholder_image_banner")
                }else{
                    cell.img.sd_setImage(with: dict["event_images"].url, placeholderImage: UIImage(named: "placeholder_image_banner"), options: .lowPriority, completed: nil)
                }
            }
            else
            {
                cell.img.image = mainIcon//UIImage(named: "placeholder_image_banner")
            }
        }
        return cell
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
}
//MARK: - API calling

extension EventDetailsVC {
    func getCulturalHighlightDetails(id:String) {
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(GetHighlightDetails)\(id)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [String:String]()
            
            
            //print("parma \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                //if let jsn = respones.value?.rawString() {
                if let jsn = respones.value
                {
                    print("json \(jsn)")
                    //let json = JSON(parseJSON: jsn)
                    if jsn["highlights"].arrayValue.count != 0
                    {
                        var dict = jsn["highlights"][0]["highlight"]
                        if dict["main_image"].stringValue != nil
                        {
                            let mainDic = dict["main_image"]
                            self.arrayImages.append(mainDic)
                            self.collectionImgs.isHidden = false
                            self.conllectionViewHeightConstraint.constant = 200
                            self.collectionImgs.reloadData()
                        }else{
                            self.collectionImgs.isHidden = true
                            self.conllectionViewHeightConstraint.constant = 0
                        }
                        self.lblEventTitle.text = dict["title"].stringValue
                        self.lblDescription.text = dict["description"].stringValue
                    }
                    else {
                        makeToast(strMessage: GlobalVariables.serverNotResponding)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    func getEventDetails(id:String) {
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(GetEventDetails)\(id)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [String:String]()
            
            
            //print("parma \(param)")
            
            self.showLoader()
            
            CommonService().GetService(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                //if let jsn = respones.value?.rawString() {
                if let jsn = respones.value
                {
                    print("json \(jsn)")
                    //let json = JSON(parseJSON: jsn)
                    if jsn["events"].exists()
                    {
                        self.dictDetails = jsn["events"][0]["event"]
                        self.lblEventTitle.text = self.dictDetails["title"].stringValue
                        
                        self.lblDescription.text = self.dictDetails["description"].stringValue
                        self.lblLocation.text = self.dictDetails["event_location"].stringValue
                        
                        // let time = dictDetails["Time"].stringValue
                         
                         //lblTime.text = strTostr(OrignalFormatter: "HH:mm", YouWantFormatter: "hh:mm a", strDate: time)
                         self.lblTime.text = self.dictDetails["start_time"].stringValue+" \(getCommonString(key: "To_key")) "+self.dictDetails["end_time"].stringValue
                         
                         
                         if self.selectedController == .fromMainEventVc
                         {
                         self.vwDate.isHidden = true
                         }
                         else
                         {
                         self.vwDate.isHidden = false
                         
                         //let value = DateToString(Formatter: "dd-MMMM-yyyy", date: self.stringDate)
                         //lblDate.text = value
                            self.lblDate.text = self.dictDetails["formatted_date"].stringValue
                         
                         }
                        
                        
                        /*if dictDetails["SignUpRequired"].boolValue == true{
                         bottomSignUpView.isHidden = false
                         SignUpButtonHeightConstraint.constant = 60
                         }else{
                         bottomSignUpView.isHidden = true
                         SignUpButtonHeightConstraint.constant = 0
                         }*/
                    }
                    else {
                        makeToast(strMessage: GlobalVariables.serverNotResponding)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    func getGetICSCalandarEvent(id:String) {
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(GetICSCalandarEvent)\(id)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            var param =  [String:String]()
            param["id"] = id
            
            //print("parma \(param)")
            
           // self.showLoader()
            
            CommonService().GetICSFILEService(url: kURL, param: param, completion: { (respones) in
                
                //self.hideLoader()
                print("response-->>",respones)
                //if let jsn = respones.value?.rawString() {
                if let jsn = respones.value
                {
                    print("json \(jsn)")
                    //let json = JSON(parseJSON: jsn)
                   /* if jsn["highlights"].arrayValue.count != 0
                    {
                        var dict = jsn["highlights"][0]["highlight"]
                        if dict["main_image"].stringValue != nil
                        {
                            let mainDic = dict["main_image"]
                            self.arrayImages.append(mainDic)
                            self.collectionImgs.isHidden = false
                            self.conllectionViewHeightConstraint.constant = 200
                            self.collectionImgs.reloadData()
                        }else{
                            self.collectionImgs.isHidden = true
                            self.conllectionViewHeightConstraint.constant = 0
                        }
                        self.lblEventTitle.text = dict["title"].stringValue
                        self.lblDescription.text = dict["description"].stringValue
                    }
                    else {
                        makeToast(strMessage: GlobalVariables.serverNotResponding)
                    }*/
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
    }
    
    
    
}


