//
//  ImgCollectionCell.swift
//  Astute
//
//  Created by YASH on 22/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class ImgCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var img: UIImageView!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
