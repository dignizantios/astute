//
//  UpcomingDeadlinesCell.swift
//  Astute
//
//  Created by Abhay on 01/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class UpcomingDeadlinesCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.font = themeFont(size: 16, fontname: .bold)
        lblTitle.textColor = UIColor.appThemeStrongRedColor
        lblTime.font = themeFont(size: 12, fontname: .light)
        lblTime.textColor = UIColor.appThemeDarkYanColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
