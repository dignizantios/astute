//
//  UpcomingHighlightsVC.swift
//  Astute
//
//  Created by Abhay on 01/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage

class UpcomingHighlightsVC: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblUpcomingList: UITableView!
    
    //MARK: - Variable
    var arrayUpcomingListing : [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblUpcomingList.register(UINib(nibName: "UpcomingDeadlinesCell", bundle: nil), forCellReuseIdentifier: "UpcomingDeadlinesCell")
        tblUpcomingList.tableFooterView = UIView()
        tblUpcomingList.delegate = self
        tblUpcomingList.dataSource = self
        self.setupUI()
        self.getAllCulturalHighlightList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Upcoming_Deadlines_key"))
    }
    func setupUI() {
        
        
        if let userSelectedColorData = Defaults.object(forKey: "backgroundColor") as? NSData {
            if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with: userSelectedColorData as Data) as? UIColor {
//                tblUpcomingList.backgroundColor = userSelectedColor
//                vwBack.backgroundColor = userSelectedColor
                
                tblUpcomingList.backgroundColor = UIColor.white
                vwBack.backgroundColor = UIColor.white

            }
        }
        
        lblTitle.text = getCommonString(key: "Upcoming_Deadlines_key")
        lblTitle.font = themeFont(size: 20, fontname: .bold)
    }

}
//MARK: - TableView data Source

extension UpcomingHighlightsVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayUpcomingListing.count > 0 ?  self.arrayUpcomingListing.count : 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingDeadlinesCell") as! UpcomingDeadlinesCell
        
        cell.tag = indexPath.row
        // print("dic-->",self.arrayCulturalListing[indexPath.row])
        var dict = self.arrayUpcomingListing[indexPath.row]["deadline"]
        cell.lblTitle.text = dict["title"].stringValue
        cell.lblTime.text = dict["deadline_date"].stringValue
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "EventDetailsVC") as! EventDetailsVC
        obj.isUpcomingDeadline = true
        obj.dictDetails = self.arrayUpcomingListing[indexPath.row]["deadline"]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}

//MARK: - APi calling

extension UpcomingHighlightsVC
{
    
    func getAllCulturalHighlightList()
    {
        
        var kURL = String()
        kURL = "\(GlobalVariables.webApiURL)\(GetUpcomingDeadlines)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  [String:String]()
            
            
            //print("parma \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                
                self.hideLoader()
                //if let jsn = respones.value?.rawString() {
                if let jsn = respones.value
                {
                    print("json \(jsn)")
                    //let json = JSON(parseJSON: jsn)
                    if jsn["deadlines"].arrayValue.count != 0
                    {
                        self.arrayUpcomingListing = jsn["deadlines"].arrayValue
                    }
                    else {
                        makeToast(strMessage: GlobalVariables.serverNotResponding)
                    }
                }
                else{
                    makeToast(strMessage:  GlobalVariables.serverNotResponding)
                }
                self.tblUpcomingList.reloadData()
            })
        }
        else
        {
            makeToast(strMessage: GlobalVariables.networkMsg)
        }
        
    }
    
}
