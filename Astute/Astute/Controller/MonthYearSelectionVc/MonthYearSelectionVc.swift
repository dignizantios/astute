//
//  MonthYearSelectionVc.swift
//  Astute
//
//  Created by YASH on 21/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit


public protocol MonthYearPickerDelegate
{
    func selectedMonthYear(monthName : String,monthInt : Int,yearName : String)
}


class MonthYearSelectionVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var pickerMonthYear: UIPickerView!
    @IBOutlet weak var btnDone: CustomButton!
    @IBOutlet weak var btnCancel: CustomButton!
    
    //MARK: - Variable
    
     var pickerDelegate : MonthYearPickerDelegate?
    
    let monthArray = ["January",
                      "February",
                      "March",
                      "April",
                      "May",
                      "June",
                      "July",
                      "August",
                      "September",
                      "October",
                      "November",
                      "December"]
    
    var yearArray = [Int]()
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 1970..<2070
        {
            yearArray.append(i)
        }
        
        self.setupUI()
        
    }
    
}

//MARK: - IBAction

extension MonthYearSelectionVc
{
    
    func setupUI()
    {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let nameOfMonth = dateFormatter.string(from: now)
        
        for i in 0..<monthArray.count
        {
            if nameOfMonth == monthArray[i]
            {
                pickerMonthYear.selectRow(i, inComponent: 0, animated: false)
                break
            }
        }
        
        dateFormatter.dateFormat = "YYYY"
        let year = Int(dateFormatter.string(from: now))
        
        for i in 0..<yearArray.count
        {
            if year == yearArray[i]
            {
                pickerMonthYear.selectRow(i, inComponent: 1, animated: false)
                break
            }
        }
        
        
        btnDone.setupThemeButtonUI()
        btnCancel.setupThemeButtonUI()
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
        pickerDelegate?.selectedMonthYear(monthName: monthArray[pickerMonthYear.selectedRow(inComponent: 0)], monthInt: pickerMonthYear.selectedRow(inComponent: 0), yearName: "\(yearArray[pickerMonthYear.selectedRow(inComponent: 1)])")
        
    }
    
}



extension MonthYearSelectionVc: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
       if component == 0
       {
            return monthArray.count
       }
        else if component == 1
       {
            return yearArray.count
       }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return monthArray[row]
        case 1:
            return "\(yearArray[row])"
        default:
            return nil
        }
    }
    
}

