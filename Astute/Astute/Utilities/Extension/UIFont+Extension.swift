//
//  UIFont+Extension.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
//    case regular = "OpenSans"
//    case semibold = "OpenSans-Semibold"
//    case light = "OpenSans-Light"
    
//    case regular = "OpenSans"
    case bold = "MetroNovaPro-Bold"
    case light = "MetroNovaPro-Light"

}

extension UIFont
{

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    print("fontname",fontname)
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
