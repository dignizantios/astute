//
//  UIColor+Extension.swift
//  Astute
//
//  Created by YASH on 19/12/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

extension UIColor {
    
//    static var appThemePurpleColor: UIColor { return UIColor.init(displayP3Red: 8/255, green: 116/255, blue: 170/255, alpha: 1.0) } /*== appThemeStrongRedColor  */
    
    static var appThemeLightGrayColor: UIColor { return UIColor.init(displayP3Red:
        170/255, green: 170/255, blue: 170/255, alpha: 1.0) }
    
    static var appThemeBlackColor: UIColor { return UIColor.init(displayP3Red:
        0/255, green: 0/255, blue: 0/255, alpha: 1.0) }
    
    static var appThemeLightPurpleColor: UIColor { return UIColor.init(displayP3Red:
        8/255, green: 116/255, blue: 170/255, alpha: 0.05) }
    
    static var appThemeWhiteColor: UIColor { return UIColor.init(displayP3Red:
        255/255, green: 255/255, blue: 255/255, alpha: 0.2) }
    
    static var appThemeVeryDarkDesautaredOrangeColor : UIColor { return UIColor.init(displayP3Red:
        95/255, green: 73/255, blue: 56/255, alpha: 0.2) }
    
    static var appThemeStrongRedColor : UIColor { return UIColor.init(displayP3Red: 216/255, green: 79/255, blue: 39/255, alpha: 1.0) }
    
    static var appThemeDarkYanColor : UIColor { return UIColor.init(displayP3Red: 36/255, green: 143/255, blue: 170/255, alpha: 1.0) }
    
    static var appThemeLightGrayishOrangColor : UIColor { return UIColor.init(displayP3Red: 245/255, green: 236/255, blue: 217/255, alpha: 1.0) }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
   

