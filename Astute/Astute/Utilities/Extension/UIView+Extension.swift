//
//  UIView+Extension.swift
//  Astute
//
//  Created by YASH on 24/10/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: targetSize.width, height: targetSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
