//
//  UIButton + Extension.swift
//  Astute
//
//  Created by YASH on 25/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit


extension UIButton
{
    
    func setupThemeButtonUI()
    {
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor.appThemeStrongRedColor
        self.titleLabel?.font = themeFont(size: 15, fontname: .light)
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
        
    }
    
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
    }
    
}
